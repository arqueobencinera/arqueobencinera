from django.conf.urls import url, include

from .views import *

urlpatterns = [
    # CRUD precio
    url(r'^compra/crear', CompraCreate.as_view(), name='compra_crear'),
    url(r'^compra/editar/(?P<pk>\d+)$', CompraUpdate.as_view(), name='compra_editar'),
    url(r'^compra/eliminar/(?P<pk>\d+)$', CompraDelete.as_view(), name='compra_eliminar'),
    url(r'^compra/lista', Compralist.as_view(), name='compra_lista'),
]