from .models import *
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
import django_tables2 as tables


class CompraTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})
    class Meta:
        model = Compra
        template = 'django_tables2/bootstrap.html'
        exclude = ('id','estado')
    def __init__(self, *args, **kwargs):
        super(CompraTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar')
        self.delete_text = _('Eliminar')
        self.report_text = _('Reporte')
    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="far fa-edit"></i> {1}
            </a>
            <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{3}">
                <i class="fa fa-trash"></i> {3}
            </a>
            <a href="{4}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{5}">
                <i class="far fa-file-pdf"></i> {5}
            </a>
            '''.format(

                reverse_lazy('compra:compra_editar', kwargs={'pk': record.pk}),
                'Modificar',
                reverse_lazy('compra:compra_eliminar', kwargs={'pk': record.pk}),
                'Eliminar',
                reverse_lazy('reporte:reporte_compra', kwargs={'pk': record.pk}),
                'reporte'
            )
        )