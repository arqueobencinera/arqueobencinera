from django import forms
from .models import *
from datetimewidget.widgets import DateWidget
from datetime import datetime,timedelta
from precio.models import *
from django_filters import FilterSet
import django_filters
import django_filters as filters

class CompraForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(CompraForm, self).__init__(*args,**kwargs)
        self.fields['precio'].queryset = PrecioHistorico.objects.exclude(id=0).order_by('-id')

    class Meta:
        model = Compra
        fields = [
            'precio',
            'fecha_emision',
            'fecha_descarga',
            'patente',
            'nombre_chofer',
            'oc93',
            'oc97',
            'diesel',
        ]
        widgets = {
            'precio': forms.Select(attrs={'class': 'form-control'}),
            'fecha_emision': DateWidget(attrs={'id': "fecha_emision"}, usel10n=True, bootstrap_version=3),
            'fecha_descarga': DateWidget(attrs={'id': "fecha_descarga"}, usel10n=True, bootstrap_version=3),
            'patente': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej: KKCK77'}),
            'nombre_chofer': forms.TextInput(attrs={'class': 'form-control'}),
            'oc93': forms.NumberInput(attrs={'class': 'form-control'}),
            'oc97': forms.NumberInput(attrs={'class': 'form-control'}),
            'diesel': forms.NumberInput(attrs={'class': 'form-control'}),
        }
    def clean_fecha_emision(self):
        fecha = datetime.today()
        max = timedelta(days=30)
        min = timedelta(days=-30)
        fechamin = fecha +min
        fechamax = fecha + max

        fecha_emision = self.cleaned_data['fecha_emision']

        if fecha_emision > fechamax.date() or fecha_emision < fechamin.date():
            raise forms.ValidationError('Fecha fuera de rango')
        else:
            return self.cleaned_data['fecha_emision']
    def clean(self):
        cleaned_data = super(CompraForm, self).clean()
        fecha = cleaned_data.get("fecha_emision")
        print(fecha)
        #fechadt = datetime.strptime(str(fecha), '%Y-%m-%d')
        max = timedelta(days=3)
        fechamin = fecha
        fechamax = fecha + max
        print(fechamin)
        print(fechamax)
        fecha_descarga = cleaned_data.get("fecha_descarga")
        print(fecha_descarga)
        if fecha_descarga > fechamax or fecha_descarga < fechamin:
            raise forms.ValidationError('Fecha fuera de rango')
        else:
            return cleaned_data

class CompraDeleteForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(CompraDeleteForm, self).__init__(*args,**kwargs)
        self.initial['estado']='Inactivo'

    class Meta:
        model = Compra
        exclude =['fecha_emision','fecha_descarga','patente','nombre_chofer','oc93','oc97','diesel','precio']

        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control','style':'visibility:hidden;'}),
        }


class SearchCompraForm(FilterSet):
    class Meta:
        model = Compra
        fields = {
            'patente':['icontains'],
            'nombre_chofer':['icontains']
        }




