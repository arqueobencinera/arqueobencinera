from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
import datetime
from precio.models import *
# Create your models here.


class Compra(models.Model):
    fecha_emision = models.DateField(default=datetime.date.today)
    fecha_descarga = models.DateField(default=datetime.date.today)
    patente = models.CharField(max_length=6)
    nombre_chofer = models.CharField(max_length=25,verbose_name="Nombre Chofer")
    oc93 = models.IntegerField(default=0, verbose_name='93', validators=[MaxValueValidator(10000), MinValueValidator(0)])
    oc97 = models.IntegerField(default=0, verbose_name='97', validators=[MaxValueValidator(10000), MinValueValidator(0)])
    diesel = models.IntegerField(default=0, verbose_name='Diesel', validators=[MaxValueValidator(15000), MinValueValidator(0)])
    precio = models.ForeignKey(PrecioHistorico, default=1)
    estado = models.CharField(max_length=8, choices=(('Activo', 'Activo'), ('Inactivo', 'Inactivo')), default='Activo')