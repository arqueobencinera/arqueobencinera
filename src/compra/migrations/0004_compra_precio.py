# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-11-23 21:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('precio', '0003_auto_20181106_0245'),
        ('compra', '0003_auto_20181116_1732'),
    ]

    operations = [
        migrations.AddField(
            model_name='compra',
            name='precio',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='precio.PrecioHistorico'),
        ),
    ]
