from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from django_tables2 import SingleTableView
from .tables import *
from .models import *
from django_filters.views import FilterView
from .forms import *
from .models import *
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from users.mixins import PermissionsRequiredMixin
from django.views.generic import View

class CompraCreate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,CreateView):
    model = Compra
    form_class = CompraForm
    required_permissions = ('compra.add_compra',)
    template_name = 'compra/create.html'
    success_url = reverse_lazy('compra:compra_lista')
    success_message = 'Exito'


class Compralist(FilterView, LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,SingleTableView,ListView):
    template_name = 'compra/list.html'
    model = Compra
    table_class = CompraTable
    required_permissions = ('compra.add_compra',)
    paginate_by = 20
    filterset_class = SearchCompraForm
    def get_queryset(self):
        qs1 = Compra.objects.exclude(estado='Inactivo')
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return Compra.objects.filter(qs).filter(id__in=qs1).order_by('-id')
        return Compra.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(Compralist, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context

class CompraUpdate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = Compra
    form_class = CompraForm
    required_permissions = ('compra.change_compra',)
    template_name = 'compra/update.html'
    success_url = reverse_lazy('compra:compra_lista')


class CompraDelete(UpdateView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = Compra
    form_class = CompraDeleteForm
    required_permissions = ('compra.delete_compra',)
    template_name = 'compra/delete.html'
    success_url = reverse_lazy('compra:compra_lista')


