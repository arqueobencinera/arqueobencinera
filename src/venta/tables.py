from .models import *
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
import django_tables2 as tables

class VentaTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                             attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = Venta
        template = 'django_tables2/bootstrap.html'
        exclude = ('estado', 'id')

    def __init__(self, *args, **kwargs):
        super(VentaTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar')
        self.delete_text = _('Eliminar')
    def render_options(self, value, record):
        return mark_safe(
             '''
             <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                 data-original-title="{1}">
                 <i class="far fa-file-pdf"></i> {1}
             </a>
             <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                 data-original-title="{3}">
                 <i class="fa fa-trash"></i> {3}
             </a>
             '''.format(

                 reverse_lazy('reporte:reporte_venta', kwargs={'pk': record.pk}),
                 'Reporte',
                 reverse_lazy('venta:venta_delete', kwargs={'pk': record.pk}),
                 'Eliminar'
             )
         )