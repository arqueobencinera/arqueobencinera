from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from precio.models import *
from combustibles.models import *
import datetime
# Create your models here.


class Venta(models.Model):
    fecha = models.DateField(default= datetime.date.today)
    turno = models.CharField(max_length=2,choices=(('A', 'A'),('B', 'B')),default='A')
    precio = models.ForeignKey(PrecioHistorico, default=1)
    estado = models.CharField(max_length=8,choices=(('Activo', 'Activo'),('Inactivo', 'Inactivo')),default='Activo')


class DetalleVenta(models.Model):
    tot_venta_litro = models.DecimalField(max_digits=8, decimal_places=3, default=0, validators=[MaxValueValidator(10000), MinValueValidator(0)])
    combustible = models.ForeignKey('combustibles.Combustible')
    venta = models.ForeignKey(Venta)


class Arqueo(models.Model):
    tot_efectivo = models.IntegerField(default=0,verbose_name="Total Efectivo", validators=[MaxValueValidator(20000000), MinValueValidator(0)])
    cheque = models.IntegerField(default=0,verbose_name="Cheques", validators=[MaxValueValidator(20000000), MinValueValidator(0)])
    credito = models.IntegerField(default=0,verbose_name="Creditos", validators=[MaxValueValidator(20000000), MinValueValidator(0)])

    tarjecredebtbk = models.IntegerField(default=0,verbose_name="Tarjeta credito TBK", validators=[MaxValueValidator(20000000), MinValueValidator(0)])
    tarjedebtbk = models.IntegerField(default=0,verbose_name="Tarjeta debito TBK", validators=[MaxValueValidator(20000000), MinValueValidator(0)])
    subtotaltarje = models.IntegerField(verbose_name="Sub-Total Tarjetas", default=0)
    tot_ingreso = models.IntegerField(verbose_name="Total Ingreso", default=0)
    venta = models.ForeignKey(Venta,null=True,blank=True)

    def __str__(self):
        return self.tot_ingreso
