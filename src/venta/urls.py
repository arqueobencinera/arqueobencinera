from django.conf.urls import url, include
from .models import *
from .views import *
from .models import *
urlpatterns = [

    # CRUD Ingreso de Combustibles
    # url(r'^venta/crear', VentaCreateView.as_view(), name='Venta_crear'),
     url(r'^venta/listar$', VentaList.as_view(), name="listar_venta"),
     url(r'^venta/crear$', CrearVenta.as_view(), name="crear_venta"),
     url(r'^venta/editar/(?P<pk>\d+)$', VentaUpdate.as_view(), name='venta_editar'),
     url(r'^venta/eliminar/(?P<pk>\d+)$', Deleteventa.as_view(), name='venta_delete'),
]