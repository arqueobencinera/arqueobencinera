from django.shortcuts import render, redirect
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import *
from .tables import *
from django_tables2 import SingleTableView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from users.mixins import PermissionsRequiredMixin
from django.db.models import Q
from django_filters.views import FilterView
class CrearVenta(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,CreateView):
    model = Venta
    second_model = Arqueo
    template_name = 'venta/create.html'
    form_class = VentaForm
    second_form_class = ArqueoForm
    required_permissions = ('venta.add_venta',)

    success_url = reverse_lazy('venta:listar_venta')
    def get(self, request, *args, **kwargs):
        """Primero ponemos nuestro object como nulo, se debe tener en
        cuenta que object se usa en la clase CreateView para crear el objeto"""
        self.object = None
        # Instanciamos el formulario de la Compra que declaramos en la variable form_class
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form2 = self.get_form(ArqueoForm)
        # Instanciamos el formset
        detalle_orden_venta_formset = DetalleVentaFormSet()
        # Renderizamos el formulario de la compra y el formset
        return self.render_to_response(self.get_context_data
        (form=form,detalle_venta_form_set=detalle_orden_venta_formset,form2=form2))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        detalle_venta_form_set = DetalleVentaFormSet(request.POST)
        if form.is_valid() and detalle_venta_form_set.is_valid() and form2.is_valid():
            self.object = form.save()
            detalle_venta_form_set.instance = self.object
            detalle_venta_form_set.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form,detalle_venta_form_set=detalle_venta_form_set,form2=form2))


class VentaList(FilterView,SingleTableView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    template_name = 'venta/list.html'
    model = Venta
    required_permissions = ('venta.add_venta',)
    table_class = VentaTable
    filterset_class = SearchVentaForm
    paginate_by = 20
    def get_queryset(self):
        qs1 = Venta.objects.exclude(estado='Inactivo')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return Venta.objects.filter(qs).filter(id__in=qs1).order_by('-id')

        return Venta.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(VentaList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context


class VentaUpdate(UpdateView):
    model = Venta
    second_form_class = ArqueoForm
    form_class = VentaForm
    second_model = Arqueo

    template_name = 'Venta/update.html'
    success_url = reverse_lazy('venta:listar_venta')
    #required_permissions = ('prestamos.change_prestamo',)


    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        arqueo = self.second_model.objects.get(venta_id=self.object.id)
        form2 = self.second_form_class(instance=arqueo)
        detallesV = DetalleVenta.objects.filter(venta_id=self.object).order_by('pk')
        detalles_dataV = []
        for detalleV in detallesV:
            d = {'combustible': detalleV.combustible,
                 'tot_venta_litro':detalleV.tot_venta_litro}
            detalles_dataV.append(d)

        detalle_venta_form_set = DetalleVentaFormSet(initial=detalles_dataV)
        return self.render_to_response(self.get_context_data(form=form,
                                                             detalle_venta_form_set=detalle_venta_form_set,form2=form2))
    # , detalle_alimento_form_set=detalle_alimento_form_set

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        detalle_venta_form_set = DetalleVentaFormSet(request.POST)
        # detalle_alimento_form_set = DetalleAlimentoFormSet(request.POST)
        # and detalle_alimento_form_set.is_valid()
        # , detalle_alimento_form_set
        # , detalle_alimento_form_set
        if form.is_valid() and detalle_venta_form_set.is_valid():
            return self.form_valid(form, detalle_venta_form_set)
        else:
            return self.form_invalid(form, detalle_venta_form_set)
    # , detalle_alimento_form_set

    def form_valid(self, form, detalle_venta_form_set):
        self.object = form.save()
        detalle_venta_form_set.instance = self.object
        # detalle_alimento_form_set.instance = self.object
        DetalleVenta.objects.filter(venta_id=self.object).delete()
        # DetalleAlimento.objects.filter(codigo_animal=self.object).delete()
        detalle_venta_form_set.save()
        # detalle_alimento_form_set.save()
        return HttpResponseRedirect(self.success_url)
    # , detalle_alimento_form_set

    def form_invalid(self, form, detalle_venta_form_set):
        return self.render_to_response(self.get_context_data(form=form,
                                                             detalle_venta_form_set=detalle_venta_form_set))



class Deleteventa(UpdateView,PermissionsRequiredMixin,SuccessMessageMixin,LoginRequiredMixin):
    model = Venta
    form_class = VentaDeleteForm
    template_name = 'venta/Deleteview.html'
    success_url = reverse_lazy('venta:listar_venta')


