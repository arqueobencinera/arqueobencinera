from django import forms
from .models import *
from combustibles.models import *
from precio.models import *
from datetimewidget.widgets import DateWidget
from django_filters import FilterSet

import django_filters
from django.forms.models import inlineformset_factory,modelform_factory
class VentaForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(VentaForm, self).__init__(*args,**kwargs)
        self.fields['precio'].queryset = PrecioHistorico.objects.exclude(id=0).order_by('-id')

    class Meta:
        model = Venta
        fields = ['precio','turno']
        labels = {
            'precio': 'Periodo de Precios',
        }
        widgets = {
            'precio': forms.Select(attrs={'class': 'form-control'}),
            'turno': forms.Select(attrs={'class': 'form-control'}),
            'estado': forms.Select(attrs={'class': 'form-control', 'style':'visibility:hidden;'}),
        }
class VentaDeleteForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(VentaDeleteForm, self).__init__(*args,**kwargs)
        self.initial['estado']='Inactivo'



    class Meta:
        model = Venta
        exclude =['fecha','turno','precio']

        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control','style':'visibility:hidden;'}),
        }

class ArqueoForm(forms.ModelForm):
    class Meta:
        model = Arqueo
        exclude = ['venta']
        fields = '__all__'
        widgets = {
            'tot_efectivo':forms.NumberInput(attrs={'class': 'form-control'}),
            'cheque': forms.NumberInput(attrs={'class': 'form-control'}),
            'credito': forms.NumberInput(attrs={'class': 'form-control'}),
            'tarjecredebtbk': forms.NumberInput(attrs={'class': 'form-control'}),
            'tarjedebtbk': forms.NumberInput(attrs={'class': 'form-control'}),
            'subtotaltarje': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly'}),
            'tot_ingreso': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly'}),


        }
class DetalleVentaForm(forms.ModelForm):
    class Meta:
        model = DetalleVenta
        labels = {
            'combustible': '',
            'tot_venta_litro': '',
        }
        fields = ['combustible','tot_venta_litro']
    def __init__(self, *args, **kwargs):
        super(DetalleVentaForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
    # def clean_tot_venta_litro(self):
    #     # cleaned_data = super(DetalleVentaForm, self).clean()
    #     stock93 = Combustible.objects.get(pk=1)
    #     stock97 = Combustible.objects.get(pk=3)
    #     stock95 = (stock93.stock + stock93.stock)
    #     stockd = Combustible.objects.get(pk=4)
    #     litro = self.cleaned_data['tot_venta_litro']
    #     litro1 = int(litro)
    #     combustible = self.cleaned_data.get('combustible')
    #     oc93 = 93
    #     oc95 = 95
    #     oc97 = 97
    #     ocd = "diesel"
################################posibilidad 1 no funciona####################3333
        # if str(oc93) == str(combustible) and int(stock93.stock) > litro1:
        #     raise forms.ValidationError("El valor excede el stock actual");
        # elif str(oc95) == str(combustible) and int(stock93) > litro1:
        #     print(2)
        #     raise forms.ValidationError("El valor excede el stock actual");
        # elif str(oc97) == str(combustible) and int(stock97.stock) > litro1:
        #     print(3)
        #     raise forms.ValidationError("El valor excede el stock actual");
        # elif str(ocd) == str(combustible) and int(stockd.stock) > litro1:
        #     print(4)
        #     raise forms.ValidationError("El valor excede el stock actual");
        # else:
        #     return litro
        ####################### posibilidad 2 no funciona ##############################
        # if stock97.octanaje and litro > stock97.stock and combustible != "":
        #     raise forms.ValidationError("El valor excede el stock actual")
        # elif stock95 and (((litro / 2) > stock93.stock) or (litro / 2 > stock97.stock)) and litro != "":
        #     raise forms.ValidationError("El valor excede el stock actual")
        # elif stock93.octanaje and litro > stock93.stock and litro != "":
        #     raise forms.ValidationError("El valor excede el stock actual")
        # elif stockd.octanaje and litro > stockd.stock and litro != "":
        #     raise forms.ValidationError("El valor excede el stock actual")
        # else:
        #     return litro
class SearchVentaForm(FilterSet):
    turno = django_filters.Filter('turno', label=('Turno'))
    fecha = django_filters.DateFilter('fecha', label=('Fecha'))
    class Meta:
        model = Venta
        fields = {
            'turno',
            'fecha',
        }



DetalleVentaFormSet = inlineformset_factory(Venta, DetalleVenta, form=DetalleVentaForm, extra=4)
