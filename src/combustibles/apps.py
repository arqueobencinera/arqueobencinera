from django.apps import AppConfig


class CombustiblesConfig(AppConfig):
    name = 'combustibles'
