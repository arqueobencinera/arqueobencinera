from django import forms
from .models import *
from datetimewidget.widgets import DateWidget


class CombustibleForm(forms.ModelForm):
    class Meta:
        model = Combustible
        fields = [
            'octanaje',
            'stock',
            'stock_minimo',
        ]
        widgets = {
            'octanaje': forms.TextInput(attrs={'class': 'form-control'}),
            'stock': forms.NumberInput(attrs={'class': 'form-control'}),
            'stock_minimo': forms.NumberInput(attrs={'class': 'form-control'}),

        }


# class DispensadorForm(forms.ModelForm):
#     class Meta:
#         model = Dispensador
#         fields = [
#             'nombre_disp',
#             'fecha_ult_mantencion',
#             'combustible',
#         ]
#         widgets = {
#             'nombre_disp': forms.TextInput(attrs={'class': 'form-control'}),
#             'fecha_ult_mantencion': DateWidget(attrs={'id': "fecha_ult_mantencion"}, usel10n=True, bootstrap_version=3),
#             'combustible': forms.Select(attrs={'class': 'form-control'}),
#
#         }
