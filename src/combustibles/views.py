from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from django_tables2 import SingleTableView
from .table import *
from .models import *
from .forms import *
from .models import *
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from users.mixins import PermissionsRequiredMixin
from django.views.generic import View
# Combustible
class CombustibleCreate(CreateView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = Combustible
    form_class = CombustibleForm
    template_name = 'combustibles/create.html'
    success_url = reverse_lazy('combustible:combustible_listar')
    success_message = 'Exito'

class Combustiblelist(SingleTableView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    template_name = 'combustibles/list.html'
    model = Combustible
    table_class = CombustibleTable
    paginate_by = 20

class CombustibleUpdate(UpdateView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = Combustible
    form_class = CombustibleForm
    template_name = 'combustibles/update.html'
    success_url = reverse_lazy('combustible:combustible_listar')


class CombustibleDelete(DeleteView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = Combustible
    template_name = 'combustibles/delete.html'
    success_url = reverse_lazy('combustible:combustible_listar')

#Dispensador
# class DispensadorCreate(CreateView):
#     model = Dispensador
#     form_class = DispensadorForm
#     template_name = 'combustibles/create.html'
#     success_url = reverse_lazy('combustible:dispensador_listar')
#
#     success_message = 'Exito'
# class DispensadorList(SingleTableView):
#     template_name = 'combustibles/list.html'
#     model = Dispensador
#     table_class = DispensadorTable
#     paginate_by = 20
#
# class DispensadorUpdate(UpdateView):
#     model = Dispensador
#     form_class = DispensadorForm
#     template_name = 'combustibles/update.html'
#     success_url = reverse_lazy('combustible:dispensador_listar')
#
#
# class DispensadorDelete(DeleteView):
#     model = Dispensador
#     template_name = 'combustibles/delete.html'
#     success_url = reverse_lazy('combustible:dispensador_listar')