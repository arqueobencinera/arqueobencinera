from django.conf.urls import url, include
from .models import *
from .views import *
from .models import *
urlpatterns = [

    # CRUD Ingreso de Combustibles
    url(r'^combustibles/crear', CombustibleCreate.as_view(), name='combustible_crear'),
    url(r'^combustibles/listar', Combustiblelist.as_view(), name='combustible_listar'),
    url(r'^combustibles/editar/(?P<pk>\d+)$', CombustibleUpdate.as_view(), name='combustible_editar'),
    url(r'^combustibles/eliminar/(?P<pk>\d+)$', CombustibleDelete.as_view(), name='combustible_eliminar'),
    # CRUD Ingreso de Dispensador
    # url(r'^dispensador/crear', DispensadorCreate.as_view(), name='dispensador_crear'),
    # url(r'^dispensador/listar', DispensadorList.as_view(), name='dispensador_listar'),
    # url(r'^dispensador/editar/(?P<pk>\d+)$', DispensadorUpdate.as_view(), name='dispensador_editar'),
    # url(r'^dispensador/eliminar/(?P<pk>\d+)$', DispensadorDelete.as_view(), name='dispensador_eliminar'),


]