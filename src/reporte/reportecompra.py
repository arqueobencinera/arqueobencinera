from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from precio.models import PrecioHistorico
from venta.models import *
from combustibles.models import *
from compra.models import *
from django.views.generic import View
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.db import *
from reportlab.platypus import Paragraph, TableStyle, Table, SimpleDocTemplate, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm, inch
from reportlab.lib.styles import getSampleStyleSheet
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.lib.pagesizes import letter, landscape, A4
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from reportlab.rl_config import defaultPageSize
PAGE_HEIGHT=A4[1]; PAGE_WIDTH=A4[0]
class InformeCompra(View):

    def cabecera(self, pdf):
        imagen = settings.MEDIA_ROOT + '\img\petro.jpg'
        pdf.drawImage(imagen, 430, 730, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        Title = "INFORME DE COMPRAS"
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
        Resumen = "Resumen de Compras"
        pdf.setFont("Helvetica", 12)
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 195, Resumen)

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        # pdf_name = "informe_compras.pdf"
        # response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        pk = self.kwargs.get('pk', 0)
       # qslitro = Venta.objects.filter(id=).only("tot_venta_litro")
        encabezados = ('Combustible', '93', '97', 'Diesel')
        detalles = [('Litros',Compra.oc93, Compra.oc97, Compra.diesel)
                     for Compra in Compra.objects.filter(pk=pk)]
        detalle_orden = Table([encabezados] + detalles, colWidths=[3 * cm, 3 * cm, 3 * cm, 3 * cm])
        detalle_orden.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        ##############---------------####################
        compra = Compra.objects.get(pk=pk)
        price = PrecioHistorico.objects.get(compra=compra.id)
        final93 = price.oc93 - price.margen
        g93 = compra.oc93 * final93
        final97 = price.oc97 - price.margen
        g97 = compra.oc97 * final97
        finaldie = price.diesel - price.margen
        gdie = compra.diesel * finaldie
        #print(price.oc93)

        encabezados1 = ('Total 93','Total 97', 'Total diesel')
        detalles1 = [(g93, g97, gdie)
                     for Compra in Compra.objects.filter(pk=pk)]
        detalle_orden1 = Table([encabezados1] + detalles1, colWidths=[3 * cm, 3 * cm, 3 * cm])
        detalle_orden1.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))

        detalles2 = [('Fecha Emision: ' + str(Compra.fecha_emision), 'Chofer: ' + Compra.nombre_chofer)
                     for Compra in Compra.objects.filter(pk=pk)]
        detalles22 = [('Fecha Descarga: ' + str(Compra.fecha_descarga), 'Patente: ' + Compra.patente)
                     for Compra in Compra.objects.filter(pk=pk)]
        detalle_orden2 = Table(detalles2 + detalles22, colWidths=[13 * cm, 13 * cm, 10 * cm, 10 * cm])

        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 110, 600)
        detalle_orden1.wrapOn(pdf, 800, 600)
        detalle_orden1.drawOn(pdf, 195, 563.9)
        detalle_orden2.wrapOn(pdf, 800, 600)
        detalle_orden2.drawOn(pdf, 30, 685)
        pdf.setPageSize(A4)

    template_name = 'reporte/prueba.html'
    success_url = reverse_lazy('home:dashboard')
