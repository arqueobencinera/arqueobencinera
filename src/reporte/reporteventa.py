﻿from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from precio.models import PrecioHistorico
from venta.models import *
from combustibles.models import *
from compra.models import *
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.db import *
from reportlab.platypus import Paragraph, TableStyle, Table, SimpleDocTemplate, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm, inch
from reportlab.lib.styles import getSampleStyleSheet
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.lib.pagesizes import letter, landscape, A4
from django.http import HttpResponse

from django.contrib.auth.decorators import login_required
from reportlab.rl_config import defaultPageSize
PAGE_HEIGHT=A4[1]; PAGE_WIDTH=A4[0]


class InformeVenta(View):
    def cabecera(self, pdf):
        imagen = settings.MEDIA_ROOT + '\img\petro.jpg'
        pdf.drawImage(imagen, 430, 730, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        Title = "INFORME DE VENTAS"
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
        Venta = "Resumen de Ventas"
        pdf.setFont("Helvetica", 12)
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 285, Venta)
        Arqueo = "Arqueo de Caja"
        pdf.setFont("Helvetica", 12)
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 445, Arqueo)
        # pdf.setFont("Helvetica", 14)
        # pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        # pdf_name = "informe_ventas.pdf"
        # response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        pk = self.kwargs.get('pk', 0)
        venta = Venta.objects.get(pk=pk)
        cuantoDetalle = DetalleVenta.objects.filter(venta=venta.id)
        precio2 = PrecioHistorico.objects.filter(id=venta.precio_id)[0]
        # vendido = DetalleVenta.objects.get(tot_venta_litro=venta.id)
        #historico = PrecioHistorico.objects.filter().order_by("-id")[0]
        x = len(cuantoDetalle)
        totalfinal93 = 0
        totalfinal95 = 0
        totalfinal97 = 0
        totalfinaldie = 0
        total93 = 0
        total95 = 0
        total97 = 0
        totaldie = 0


        i = 0
        while i <= x - 1:
            detalle1 = DetalleVenta.objects.filter(venta=venta.id)[i]
            if detalle1.combustible_id == 1:
                total93 = detalle1.tot_venta_litro * precio2.oc93
                totalfinal93 = totalfinal93 + total93
                i += 1
                # print(total93)
            elif detalle1.combustible_id == 2:
                total95 = detalle1.tot_venta_litro * precio2.oc95
                totalfinal95 = totalfinal95 + total95
                i += 1

                #print(total97)
            elif detalle1.combustible_id == 3:
                total97 = detalle1.tot_venta_litro * precio2.oc97
                totalfinal97 = totalfinal97 + total97
                i += 1

                #print(totaldie)
            else:
                totaldie = detalle1.tot_venta_litro * precio2.diesel
                totalfinaldie = totalfinaldie + totaldie
                i += 1
                #print(total95)

        encabezados = ('Producto',)
        detalles = [(Combustible.octanaje,)
                    for Combustible in Combustible.objects.all().order_by('id')]

        detalle_orden = Table([encabezados] + detalles,
                              colWidths=[1.9 * cm])
        detalle_orden.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))


        ######################################################
        qslitro = Venta.objects.filter(id=pk).only("tot_venta_litro")
        encabezados2 = ('Litros',)
        i = 0

        prueba = DetalleVenta.objects.filter(venta_id=qslitro)
        print(prueba[0].id)
        print(len(prueba))
        detalles2=0
        if len(prueba) == 1:
            a = int(prueba[0].tot_venta_litro)
            b = "0"
            c = "0"
            d = "0"
        elif len(prueba) == 2:
            a = int(prueba[0].tot_venta_litro)
            b = int(prueba[1].tot_venta_litro)
            c = "0"
            d = "0"
        elif len(prueba) == 3:
            a = int(prueba[0].tot_venta_litro)
            b = int(prueba[1].tot_venta_litro)
            c = int(prueba[2].tot_venta_litro)
            d = "0"
        elif len(prueba) == 4:
            a = int(prueba[0].tot_venta_litro)
            b = int(prueba[1].tot_venta_litro)
            c = int(prueba[2].tot_venta_litro)
            d = int(prueba[3].tot_venta_litro)
        detalles2=[(str(a),),(str(b),),(str(c),),(str(d),)]
        print(detalles2[0])
        print(detalles2[1])
        print(detalles2[2])
        print(detalles2[3])

        # while i < len(prueba):
        #     if prueba[i].id != None:
        #         detalles2[i]=[(str(int(prueba[i].tot_venta_litro)),)]
        #         i += 1
        # while i < 4:
        #     detalles2.append("0")
        #     i += 1

        detalle_orden2 = Table([encabezados2] + detalles2, colWidths=[2.3 * cm])
        detalle_orden2.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        ########################################################
        qsprecio = Venta.objects.filter(id=pk).only("precio_id")
        det93 = PrecioHistorico.objects.values_list('oc93').get(venta=qsprecio)
        det95 = PrecioHistorico.objects.values_list('oc95').get(venta=qsprecio)
        det97 = PrecioHistorico.objects.values_list('oc97').get(venta=qsprecio)
        detdie = PrecioHistorico.objects.values_list('diesel').get(venta=qsprecio)
        encabezados3 = ('$ Unitario',)
        detalles3 = [det93, det95, det97, detdie]
        detalle_orden3 = Table([encabezados3] + detalles3, colWidths=[2.2 * cm])
        detalle_orden3.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        # #################################################
        precio = PrecioHistorico.objects.get(venta=qsprecio)
        encabezados4 = ['Total']
        tot93 = total93 * 1
        t93 = [(str(int(tot93)))]
        tot95 = total95 * 1
        t95 = [(str(int(tot95)))]
        tot97 = total97 * 1
        t97 = [(str(int(tot97)))]
        totdie = totaldie * 1
        tdie = [(str(int(totdie)))]
        detalles4 = [t93, t95, t97, tdie]
        detalle_orden4 = Table([encabezados4] + detalles4, colWidths=[2.8 * cm])
        detalle_orden4.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        #############################################
        #qsarqueo = Arqueo.objects.get(pk=venta.precio_id)
        encabezados5 = ['Total Efectivo', 'Cheques', 'Credito c/ guia', 'T. Cred. Debito', 'T. Debito', 'Sub. Tot. T. Cred', 'TOTAL INGRESOS']
        detalles5 = [(Arqueo.tot_efectivo, Arqueo.cheque, Arqueo.credito, Arqueo.tarjecredebtbk, Arqueo.tarjedebtbk, Arqueo.subtotaltarje, Arqueo.tot_ingreso)
                     for Arqueo in Arqueo.objects.filter(id=pk)]
        detalle_orden5 = Table([encabezados5] + detalles5, colWidths=[2.5 * cm, 2.3 * cm, 2.6 * cm, 2.5 * cm, 2.5 * cm, 2.8 * cm, 3.5 * cm])
        detalle_orden5.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 9),
            ]
        ))
        qsturno = Venta.objects.filter(id=pk).only("turno")
        qsfecha = Venta.objects.filter(id=pk).only("fecha")
        detalles6 = [('Turno ' + Venta.turno + ' /', Venta.fecha)
                     for Venta in Venta.objects.filter(id=qsturno&qsfecha)]
        detalle_orden6 = Table(detalles6, colWidths=[1.5 * cm, 1 * cm])

        encabezados7 = ('Total Vendido', 'Total Ingresos', 'Diferencia')
        suma = int(t93[0])+int(t95[0])+int(t97[0])+int(tdie[0])
        qss = Arqueo.objects.get(id=pk)
        ingreso = qss.tot_ingreso
        diferencia = ingreso - suma
        totalfinal = [suma, ingreso, diferencia]
        print(totalfinal)
        x = totalfinal
        v = str(totalfinal)
        detalles7 = [x]
        detalle_orden7 = Table([encabezados7] + detalles7,
                               colWidths=[4 * cm, 4 * cm, 4 * cm])
        detalle_orden7.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 9),
            ]
        ))


        ###############-----DIBUJO DE REPORTES-----#################
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 150, 600)
        detalle_orden2.wrapOn(pdf, 800, 600)
        detalle_orden2.drawOn(pdf, 204, 600)
        detalle_orden3.wrapOn(pdf, 800, 600)
        detalle_orden3.drawOn(pdf, 269.517, 600)
        detalle_orden4.wrapOn(pdf, 800, 600)
        detalle_orden4.drawOn(pdf, 332.114, 600)
        ################-----ARQUEO-----##################
        detalle_orden5.wrapOn(pdf, 800, 600)
        detalle_orden5.drawOn(pdf, 30, 470)
        ################-----ARQUEO^-----##################
        detalle_orden6.wrapOn(pdf, 800, 600)
        detalle_orden6.drawOn(pdf, 480, 728)
        detalle_orden7.wrapOn(pdf, 800, 600)
        detalle_orden7.drawOn(pdf, 110, 320)
        pdf.setPageSize(A4)

    template_name = 'reporte/prueba.html'
    success_url = reverse_lazy('home:dashboard')
