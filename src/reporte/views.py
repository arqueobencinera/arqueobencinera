from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import *
from precio.models import PrecioHistorico
from venta.models import *
from combustibles.models import *
from compra.models import *
from .form import *
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.db import *
from reportlab.platypus import Paragraph, TableStyle, Table, SimpleDocTemplate, Spacer, Flowable
from reportlab.lib import colors
from reportlab.lib.units import cm, inch
from reportlab.lib.styles import getSampleStyleSheet
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.lib.pagesizes import letter, landscape, A4
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from reportlab.rl_config import defaultPageSize

from handling.models import HandlingDiario
from venta.models import DetalleVenta
from django.db.models import *
PAGE_HEIGHT=A4[1]; PAGE_WIDTH=A4[0]
width, height = A4
# Create your views here.
# def prueba(request):
#     venta = Venta.objects.get(pk=1)
#     detalles = DetalleVenta.objects.filter(venta=venta.id)
#     precio = PrecioHistorico.objects.get(pk=venta.precio_id)
#     detalle = detalles
#     for i in detalle:
#         if i.combustible_id == 1:
#             total93 = 0
#             total93 = i.tot_venta_litro * precio.oc93
#         if i.combustible_id == 2:
#             total95 = 0
#             total95 = i.tot_venta_litro * precio.oc95
#         if i.combustible_id == 3:
#             total97 = 0
#             total97 = i.tot_venta_litro * precio.oc97
#         if i.combustible_id == 4:
#             totald = 0
#             totald = i.tot_venta_litro * precio.diesel
#     totafinal =total93+total95+total97+totald
#     print(totafinal)
#
#     context = {
#
#      }
#     return render(request, "reporte/periodo.html", context)


class reporteFecha(View):

    def cabecera(self, pdf):
        imagen = settings.MEDIA_ROOT + '\img\petro.jpg'
        pdf.drawImage(imagen, 430, 730, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        Title = "INFORME DE VENTAS"
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
        # pdf.drawString(180, 750, u"INFORME DE VENTAS")
        # pdf.setFont("Helvetica", 14)
        # pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        fecha = self.kwargs.get('fecha', 0)
        fecha2 = self.kwargs.get('fecha2', 0)
        compra = obtenerCompra(fecha,fecha2)
        venta1 = obtenerVenta(fecha,fecha2)
########################################################################################
        i = 0
        detalles93 = 0
        total93 = 0
        detalles95 = 0
        total95 = 0
        detalles97 = 0
        total97 = 0
        detallesd = 0
        totald = 0
        a = 1
        while i <= len(venta1) - 1:
            if a == 1:
                try:
                    detalles93 = DetalleVenta.objects.get(venta=venta1[i].id, combustible=1)
                    total93 = detalles93.tot_venta_litro + total93
                    print(1)
                except DetalleVenta.DoesNotExist:
                    total93 = 0 + total93
            if a == 1:
                try:
                    detalles95 = DetalleVenta.objects.get(venta=venta1[i].id, combustible=2)
                    total95 = detalles95.tot_venta_litro + total95
                    print(1)
                except DetalleVenta.DoesNotExist:
                    total95 = 0 + total95
            if a == 1:
                try:
                    detalles97 = DetalleVenta.objects.get(venta=venta1[i].id, combustible=3)
                    total97 = detalles97.tot_venta_litro + total97
                    print(1)
                except DetalleVenta.DoesNotExist:
                    total97 = 0 + total97
            if a == 1:
                try:
                    detallesd = DetalleVenta.objects.get(venta=venta1[i].id, combustible=4)
                    totald = detallesd.tot_venta_litro + totald
                    print(1)
                except DetalleVenta.DoesNotExist:
                    totald = 0 + totald
            i = i + 1
            print(total93)
            print(total95)
            print(total97)
            print(totald)
#########################################################################################
        i = 0
        totalventa = 0
        while i <= len(venta1) - 1:
            totalventa = totalventa + ventaTotal(venta1[i].id)
            i = i + 1
        encabezados = ('Total Ventas',)

        detalles = [(str(int(totalventa)),)]

        detalle_orden = Table([encabezados] + detalles,
                              colWidths=[4 * cm])
        detalle_orden.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        i = 0
        valor = 0

        #total de litro comprados en dinero
        while i <= len(compra) - 1:
             valor = valor + costoCompra(compra[i].id)
             i = i +1

        encabezados2 = ('Total Compras',)
        detalles2 = [(str(int(valor)),)]

        detalle_orden2 = Table([encabezados2] + detalles2,
                              colWidths=[4 * cm])
        detalle_orden2.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))

        i = 0
        valor93 = 0
        valor97 = 0
        valord = 0
        var = compra
        while i <= len(compra) - 1:
            if var[i].oc93 != 0:
                valor93 = valor93 + var[i].oc93
            if var[i].oc97 != 0:
                valor97 = valor97 + var[i].oc97
            if var[i].diesel != 0:
                valord = valord + var[i].diesel
            i = i + 1

        encabezados3 = ('Total Litros Comprados',)
        detalles3 = [(str(float(valor93)),),(str(float(valor97)),),(str(float(valord)),)]

        detalle_orden3 = Table([encabezados3] + detalles3,
                               colWidths=[4 * cm])
        detalle_orden3.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        encabezados4 = ('Octanaje',)
        detalles4 = [(Combustible.octanaje,)
                    for Combustible in Combustible.objects.exclude(id=2).order_by('id')]

        detalle_orden4 = Table([encabezados4] + detalles4,
                              colWidths=[2.5 * cm])
        detalle_orden4.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        encabezados6 = ('Total Litros Vendidos ',)
        detalles6 = [(str(float(total93)),), (str(float(total95)),), (str(float(total97)),), (str(float(totald)),),]

        detalle_orden6 = Table([encabezados6] + detalles6,
                               colWidths=[4 * cm])
        detalle_orden6.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        encabezados5 = ('Octanaje',)
        detalles5 = [(Combustible.octanaje,)
                     for Combustible in Combustible.objects.all().order_by('id')]

        detalle_orden5 = Table([encabezados5] + detalles5,
                               colWidths=[2.5 * cm])
        detalle_orden5.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        utilidades = totalventa - valor
        encabezados7 = ('Total Utilidades',)
        detalles7 = [(utilidades,)]

        detalle_orden7 = Table([encabezados7] + detalles7,
                               colWidths=[5 * cm])
        detalle_orden7.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))


        a = [('Desde',)]
        b = [('Hasta',)]
        fechaini =Table(a + [(str(fecha),)], colWidths=[1.9 * cm])
        fechafin =Table(b + [(str(fecha2),)], colWidths=[1.9 * cm])
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 350, 525)
        detalle_orden2.wrapOn(pdf, 900, 700)
        detalle_orden2.drawOn(pdf, 350, 635)
        detalle_orden3.wrapOn(pdf, 700, 500)
        detalle_orden3.drawOn(pdf, 200.8, 610)
        detalle_orden4.wrapOn(pdf, 900, 800)
        detalle_orden4.drawOn(pdf, 130, 610)
        detalle_orden5.wrapOn(pdf, 900, 800)
        detalle_orden5.drawOn(pdf, 130, 500)
        detalle_orden6.wrapOn(pdf, 900, 800)
        detalle_orden6.drawOn(pdf, 200.8, 500)
        detalle_orden7.wrapOn(pdf, 900, 800)
        detalle_orden7.drawOn(pdf, 250, 400)
        fechaini.wrapOn(pdf,1500, 800)
        fechaini.drawOn(pdf, 412, 722)
        fechafin.wrapOn(pdf, 1500, 800)
        fechafin.drawOn(pdf, 480, 722)


class reporteHandling(View):

    def cabecera(self, pdf):
        imagen = settings.MEDIA_ROOT + '\img\petro.jpg'
        pdf.drawImage(imagen, 430, 730, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        Title = "REPORTE HANDLING"
        pdf.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
        # pdf.drawString(180, 750, u"INFORME DE VENTAS")
        # pdf.setFont("Helvetica", 14)
        # pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        pk = self.kwargs.get('fecha', 0)
        pk1 = self.kwargs.get('fecha2', 0)
        pk2 = self.kwargs.get('pk',0)
        fecha = pk
        fecha2 = pk1
        pk = pk2
        estado = HandlingCliente.objects.filter(pk = pk)

        encabezados2 = ('Fecha', 'Kilometraje', 'Patente', 'N. Chofer', 'Litros', 'Cliente')
        detalles2 = [(HandlingDiario.fecha, HandlingDiario.kilometraje, HandlingDiario.patente, HandlingDiario.nombre_chofer, HandlingDiario.litros, HandlingDiario.handlingcliente)
                     for HandlingDiario in HandlingDiario.objects.filter(fecha__range=(fecha, fecha2),handlingcliente = pk)]

        detalle_orden2 = Table([encabezados2] + detalles2, colWidths=[2.5 * cm, 2.2 * cm, 2 * cm, 3.5 * cm, 2.2 * cm, 6 * cm])
        detalle_orden2.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (9, -1), 0.5, colors.black),
                ('BACKGROUND', (0, 0), (-1, 0), colors.lightslategray),
                ('FONTSIZE', (0, 0), (-1, -1), 10),
                ('ALIGN', (0, 0), (0, 0), 'CENTER'),
            ]
        ))
        fechaini =Table([('Desde ',)] + [(str(fecha),)], colWidths=[1.9 * cm])
        fechafin =Table([('Hasta ',)] + [(str(fecha2),)], colWidths=[1.9 * cm])
        estado1 =Table([('Estado Cliente ',)] + [(str(estado[0].estado),)], colWidths=[1.9 * cm])
        x = 30
        y = 635
        pdf.setPageSize(A4)
        detalle_orden2.wrapOn(pdf, width, height)
        detalle_orden2.drawOn(pdf, x, PAGE_HEIGHT - 300)
        fechaini.wrapOn(pdf, width, height)
        fechaini.drawOn(pdf, 30, 722)
        fechafin.wrapOn(pdf, width, height)
        fechafin.drawOn(pdf, 98, 722)
        estado1.wrapOn(pdf, width, height)
        estado1.drawOn(pdf, 480, 722)



def coord(x, y, unit=1):
    x, y = x * unit, height - y * unit
    return x, y

def obtenerVenta(fecha,fecha2):
    venta = Venta.objects.filter(fecha__range=(fecha, fecha2))
    return venta


def obtenerCompra(fecha,fecha2):
    venta = Compra.objects.filter(fecha_emision__range=(fecha, fecha2))
    return venta


def costoCompra(idcompra):
    compra = Compra.objects.get(pk=idcompra)
    precio = PrecioHistorico.objects.get(pk=compra.precio_id)
    valor93 = 0
    valor97 = 0
    valord = 0
    if compra.oc93 != 0:
        valor93 = compra.oc93 * (precio.oc93-precio.margen)
    if compra.oc97!=0:
        valor97 = compra.oc97 * (precio.oc97-precio.margen)
    if compra.diesel !=0:
        valord = compra.diesel * (precio.diesel-precio.margen)
    final = valor97 + valor93 + valord
    return final


def ventaTotal(idventa):
    venta = Venta.objects.get(pk=idventa)
    detalles = DetalleVenta.objects.filter(venta=venta.id)
    precio = PrecioHistorico.objects.get(pk=venta.precio_id)
    detalle = detalles
    total93 = 0
    total95 = 0
    total97 = 0
    totald = 0
    for i in detalle:
        if i.combustible_id == 1:
            total93 = i.tot_venta_litro * precio.oc93
        if i.combustible_id == 2:
            total95 = i.tot_venta_litro * precio.oc95
        if i.combustible_id == 3:
            total97 = i.tot_venta_litro * precio.oc97
        if i.combustible_id == 4:
            totald = i.tot_venta_litro * precio.diesel
    totafinal = total93 + total95 + total97 + totald
    return totafinal


class ReporteCompraVenta(TemplateView):
    template_name = "reporte/reporte.html"


class ReporteHandling(CreateView):
    template_name = "reporte/handling.html"
    form_class = HandlingDiarioForm