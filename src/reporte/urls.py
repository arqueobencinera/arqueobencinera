from django.conf.urls import url

from .views import *
from .reporteventa import *
from .reportecompra import *


urlpatterns = [
    url(r'^reporte/venta/(?P<pk>\d+)$', InformeVenta.as_view(), name='reporte_venta'),
    url(r'^reporte/compra/(?P<pk>\d+)$', InformeCompra.as_view(), name='reporte_compra'),
    url(r'^fecha$', ReporteCompraVenta.as_view(), name='reporte'),
    url(r'^retiros$', ReporteHandling.as_view(), name='hand'),
    url(r'^reporte/compra-venta/(?P<fecha>[\w.@+-]+)/' 
        r'(?P<fecha2>[\w.@+-]+)$', reporteFecha.as_view(), name='compraventa'),
    url(r'^reporte/handling/(?P<fecha>[\w.@+-]+)/' 
        r'(?P<fecha2>[\w.@+-]+)/'
        r'(?P<pk>\d+)$', reporteHandling.as_view(), name='handling'),
]

