from django import forms
from .models import *
from datetimewidget.widgets import DateWidget
from handling.models import *
class  HandlingDiarioForm(forms.ModelForm):
    class Meta:
        model = HandlingDiario
        fields = [
            'handlingcliente',
        ]
        widgets = {
            'handlingcliente': forms.Select(attrs={'class': 'form-control'}),

        }