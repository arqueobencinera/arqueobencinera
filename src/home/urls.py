from django.conf.urls import url

from .views import HomeTemplateView, dashboard, LoginTemplateView,grafico


urlpatterns = [
    #url(r'^$', HomeTemplateView.as_view(), name='index'),
    url(r'^$', dashboard, name='dashboard'),
    url(r'^grafico$', grafico, name='grafico'),
    url(r'^login$', LoginTemplateView.as_view(), name='login'),
]
