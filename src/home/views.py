from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from precio.models import PrecioHistorico
from combustibles.models import Combustible
from venta.models import *
from django.contrib.auth.decorators import login_required
from reporte.reporteventa import *
import json
from django.http import JsonResponse
# Create your views here.


class HomeTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'home/index.html'


class LoginTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'home/login.html'


@login_required()
def dashboard(request):
    obj1 = PrecioHistorico.objects.filter().order_by("-id")[0]
    obj2 = Combustible.objects.get(pk=1)
    obj3 = Combustible.objects.get(pk=3)
    obj4 = Combustible.objects.get(pk=4)
    obj5 = Arqueo.objects.filter().order_by("-id")[0]
    obj55 = Arqueo.objects.filter().order_by("-id")[0]
    venta = Venta.objects.get(pk=obj55.id)
    cuantoDetalle = DetalleVenta.objects.filter(venta=venta.id)
    precio2 = PrecioHistorico.objects.filter(id=venta.precio_id)[0]
    x = len(cuantoDetalle)
    totalfinal93 = 0
    totalfinal95 = 0
    totalfinal97 = 0
    totalfinaldie = 0
    total93 = 0
    total95 = 0
    total97 = 0
    totaldie = 0

    i = 0
    while i <= x - 1:
        detalle1 = DetalleVenta.objects.filter(venta=venta.id)[i]
        if detalle1.combustible_id == 1:
            total93 = detalle1.tot_venta_litro * precio2.oc93
            totalfinal93 = totalfinal93 + total93
            i += 1
            # print(total93)
        elif detalle1.combustible_id == 2:
            total95 = detalle1.tot_venta_litro * precio2.oc95
            totalfinal95 = totalfinal95 + total95
            i += 1

            # print(total97)
        elif detalle1.combustible_id == 3:
            total97 = detalle1.tot_venta_litro * precio2.oc97
            totalfinal97 = totalfinal97 + total97
            i += 1

            # print(totaldie)
        else:
            totaldie = detalle1.tot_venta_litro * precio2.diesel
            totalfinaldie = totalfinaldie + totaldie
            i += 1
    tot93 = total93 * 1
    t93 = [(str(int(tot93)))]
    tot95 = total95 * 1
    t95 = [(str(int(tot95)))]
    tot97 = total97 * 1
    t97 = [(str(int(tot97)))]
    totdie = totaldie * 1
    tdie = [(str(int(totdie)))]
    suma = int(t93[0]) + int(t95[0]) + int(t97[0]) + int(tdie[0])
    total = obj5.tot_ingreso
    dif = total - suma

    #################--GRAFICO--##############
    #################--JULIO--############
    julio = Venta.objects.filter(fecha__month=7)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0
    print(len(julio))
    y = len(julio)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=julio[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=julio[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=julio[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=julio[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    julio93 = total93
    julio95 = total95
    julio97 = total97
    juliod = totald

    #################--AGOSTO--############
    ago = Venta.objects.filter(fecha__month=8)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0
    print(len(ago))
    y = len(ago)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=ago[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=ago[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=ago[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=ago[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    ago93 = total93
    ago95 = total95
    ago97 = total97
    agod = totald

    #################--SEPTIEMBRE--############
    sep = Venta.objects.filter(fecha__month=9)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0

    print(len(sep))
    y = len(sep)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=sep[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=sep[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=sep[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=sep[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    sep93 = total93
    sep95 = total95
    sep97 = total97
    sepd = totald

    #################--OCTUBRE--############
    octubre = Venta.objects.filter(fecha__month=10)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0
    print(len(octubre))
    y = len(octubre)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=octubre[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=octubre[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=octubre[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=octubre[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    octubre93 = total93
    octubre95 = total95
    octubre97 = total97
    octubred = totald

    #################--NOVIEMBRE--############
    nov = Venta.objects.filter(fecha__month=11)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0
    print(len(nov))
    y = len(nov)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=nov[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=nov[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=nov[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=nov[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    nov93 = total93
    nov95 = total95
    nov97 = total97
    novd = totald

    #################--DICIEMBRE--############
    dic = Venta.objects.filter(fecha__month=12)
    x = 0
    detalles93 = 0
    total93 = 0
    detalles95 = 0
    total95 = 0
    detalles97 = 0
    total97 = 0
    detallesd = 0
    totald = 0
    print(len(dic))
    y = len(dic)
    a = 1
    while x <= y - 1:
        if a == 1:
            try:
                detalles93 = DetalleVenta.objects.get(venta_id=dic[x].id, combustible=1)
                total93 = detalles93.tot_venta_litro + total93
            except DetalleVenta.DoesNotExist:
                total93 = 0 + total93
        if a == 1:
            try:
                detalles95 = DetalleVenta.objects.get(venta_id=dic[x].id, combustible=2)
                total95 = detalles95.tot_venta_litro + total95
            except DetalleVenta.DoesNotExist:
                total95 = 0 + total95
        if a == 1:
            try:
                detalles97 = DetalleVenta.objects.get(venta_id=dic[x].id, combustible=3)
                total97 = detalles97.tot_venta_litro + total97
            except DetalleVenta.DoesNotExist:
                total97 = 0 + total97
        if a == 1:
            try:
                detallesd = DetalleVenta.objects.get(venta_id=dic[x].id, combustible=4)
                totald = detallesd.tot_venta_litro + totald
            except DetalleVenta.DoesNotExist:
                totald = 0 + totald
        x = x + 1

    print(total93)
    print(total95)
    print(total97)
    print(totald)
    dic93 = total93
    dic95 = total95
    dic97 = total97
    dicd = totald

    context = {
        "obj1": obj1,
        "obj2": obj2,
        "obj3": obj3,
        "obj4": obj4,
        "obj5": obj5,
        "obj6": suma,
        "obj7": dif,
        "obj8": str(float(julio93)),
        "obj9": str(float(julio95)),
        "obj10": str(float(julio97)),
        "obj11": str(float(juliod)),
        "obj12": str(float(ago93)),
        "obj13": str(float(ago95)),
        "obj14": str(float(ago97)),
        "obj15": str(float(agod)),
        "obj16": str(float(sep93)),
        "obj17": str(float(sep95)),
        "obj18": str(float(sep97)),
        "obj19": str(float(sepd)),
        "obj20": str(float(octubre93)),
        "obj21": str(float(octubre95)),
        "obj22": str(float(octubre97)),
        "obj23": str(float(octubred)),
        "obj24": str(float(nov93)),
        "obj25": str(float(nov95)),
        "obj26": str(float(nov97)),
        "obj27": str(float(novd)),
        "obj28": str(float(dic93)),
        "obj29": str(float(dic95)),
        "obj30": str(float(dic97)),
        "obj31": str(float(dicd)),
    }

    return render(request, "home/dashboard.html", context)


def grafico(request):
    obj2 = Combustible.objects.get(pk=1)
    obj3 = Combustible.objects.get(pk=3)
    obj4 = Combustible.objects.get(pk=4)

    obj5 =[["93",obj2.stock],["97",obj3.stock],["Diesel",obj4.stock]]




    return JsonResponse(obj5 ,safe=False)
