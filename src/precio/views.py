from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from django_tables2 import SingleTableView
from .models import *
from django_filters.views import FilterView
from .forms import *
from .table import *
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from users.mixins import PermissionsRequiredMixin
from django.views.generic import View
# Create your views here.


class PrecioHistoricoCreate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,CreateView):
    model = PrecioHistorico
    form_class = PrecioHistoricoForm
    required_permissions = ('precio.add_preciohistorico',)
    template_name = 'precio/precio_create.html'
    success_url = reverse_lazy('precio:precio_lista')
    success_message = 'Exito'


class PrecioHistoricoUpdate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = PrecioHistorico
    form_class = PrecioHistoricoForm
    required_permissions = ('precio.change_preciohistorico',)
    template_name = 'precio/precio_update.html'
    success_url = reverse_lazy('precio:precio_lista')


class PrecioHistoricoDelete(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,DeleteView):
    model = PrecioHistorico
    required_permissions = ('precio.delete_preciohistorico',)
    template_name = 'precio/precio_delete.html'
    success_url = reverse_lazy('precio:precio_lista')


class PrecioHistoricoList(FilterView,SingleTableView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    template_name = 'precio/precio_list.html'
    model = PrecioHistorico
    table_class = PrecioTable
    filterset_class = SearchPrecioForm
    required_permissions = ('precio.add_preciohistorico',)
    paginate_by = 20
    def get_queryset(self):
        qs1 = PrecioHistorico.objects.exclude(id='0')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return PrecioHistorico.objects.filter(qs).filter(id__in=qs1).order_by('-id')

        return PrecioHistorico.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(PrecioHistoricoList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context