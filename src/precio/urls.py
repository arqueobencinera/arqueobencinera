from django.conf.urls import url, include
from .models import *
from .views import *
from .models import *
urlpatterns = [

    # CRUD precio
    url(r'^precio/crear', PrecioHistoricoCreate.as_view(), name='precio_crear'),
    url(r'^precio/editar/(?P<pk>\d+)$', PrecioHistoricoUpdate.as_view(), name='precio_editar'),
    url(r'^precio/eliminar/(?P<pk>\d+)$', PrecioHistoricoDelete.as_view(), name='precio_eliminar'),
    url(r'^precio/lista', PrecioHistoricoList.as_view(), name='precio_lista'),



]