# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-11-06 04:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('precio', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='preciohistorico',
            name='impfijo93',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impfijo95',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impfijo97',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impfijodiesel',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='imptotal93',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='imptotal95',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='imptotal97',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='imptotaldiesel',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impvar93',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impvar95',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impvar97',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='impvardiesel',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='iva93',
            field=models.DecimalField(decimal_places=4, max_digits=8, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='iva95',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='iva97',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='ivadiesel',
            field=models.DecimalField(decimal_places=3, max_digits=6, null=True),
        ),
        migrations.AlterField(
            model_name='preciohistorico',
            name='margen',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
