from .models import *
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
import django_tables2 as tables

class PrecioTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = PrecioHistorico
        template = 'django_tables2/bootstrap.html'
        exclude = (
        'id', 'margen', 'costo93', 'iva93', 'impfijo93', 'impvar93', 'imptotal93', 'costo95', 'iva95', 'impfijo95',
        'impvar95', 'imptotal95', 'costo97', 'iva97', 'impfijo97', 'impvar97', 'imptotal97', 'costodiesel', 'ivadiesel',
        'impfijodiesel', 'impvardiesel', 'imptotaldiesel')
        label = {'fechafin', 'diesel', 'oc93', 'oc95', 'oc97', 'fechaini'}


    def __init__(self, *args, **kwargs):
        super(PrecioTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar1')
        self.delete_text = _('Eliminar')


    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="far fa-edit"></i> {1}
            </a>
            <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{3}">
                <i class="fa fa-trash"></i> {3}
            </a>
            '''.format(

                reverse_lazy('precio:precio_editar', kwargs={'pk': record.pk}),
                'Modificar',
                reverse_lazy('precio:precio_eliminar', kwargs={'pk': record.pk}),
                'Eliminar'
            )
        )
# class PrecioTable(tables.Table):
#
#     class Meta:
#         model = PrecioHistorico
#         template = 'django_tables2/bootstrap.html'
#         exclude = ()
#         fields = {'fechaini','fechafin','oc93','oc95','oc97','diesel'}
#
#     def __init__(self, *args, **kwargs):
#         super(PrecioTable, self).__init__(*args, **kwargs)
#         self.edit_text = _('Modificar')
#         self.delete_text = _('Eliminar')