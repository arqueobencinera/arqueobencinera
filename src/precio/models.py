from django.db import models
from django.utils.dateformat import format
from django.core.exceptions import ValidationError

from django.core.validators import MaxValueValidator, MinValueValidator, MaxLengthValidator
# Create your models here.


class PrecioHistorico(models.Model):

    fechaini = models.DateField(null=True,verbose_name="Fecha Inicio")
    fechafin = models.DateField(null=True,verbose_name="Fecha Fin")

    costo93 = models.IntegerField(null=True, validators=[MaxValueValidator(2000), MinValueValidator(0)])
    iva93 = models.DecimalField(max_digits=8,decimal_places=4,null=True)
    impfijo93 = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    impvar93 = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    imptotal93 = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    oc93 = models.IntegerField(null=True,verbose_name="Valor 93")

    costo95 = models.IntegerField(null=True, validators=[MaxValueValidator(2000), MinValueValidator(0)])
    iva95 = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    impfijo95 = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    impvar95 = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    imptotal95 = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    oc95 = models.IntegerField(null=True,verbose_name="Valor 95")

    costo97 = models.IntegerField(null=True, validators=[MaxValueValidator(2000), MinValueValidator(0)])
    iva97 = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    impfijo97 = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(0)])
    impvar97 =models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    imptotal97 = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    oc97 = models.IntegerField(null=True,verbose_name="Valor 97")

    costodiesel = models.IntegerField(null=True, validators=[MaxValueValidator(2000), MinValueValidator(0)])
    ivadiesel = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    impfijodiesel = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    impvardiesel = models.DecimalField(max_digits=8,decimal_places=3,null=True, validators=[MaxValueValidator(100), MinValueValidator(-100)])
    imptotaldiesel = models.DecimalField(max_digits=8,decimal_places=3,null=True)
    diesel = models.IntegerField(null=True,verbose_name="Valor Diesel")

    margen = models.IntegerField(null=True, default=0, validators=[MaxValueValidator(1000), MinValueValidator(0)])


    def __str__(self):
        inicio = format(self.fechaini, 'd F Y')
        fin = format(self.fechafin, 'd F Y')
        return str("Desde:"+inicio+" ~ Hasta: "+fin)





