from lib2to3.fixes import fix_throw
from django.contrib.admin.widgets import AdminDateWidget
from django import forms
from .models import *
from datetimewidget.widgets import DateWidget
from datetime import datetime,timedelta
from django_filters import FilterSet
# def valid_marge_range(value):
#     if value < 0 or value > 1:
#         raise forms.ValidationError(u'%s is not between 0 and 1' % value)


class PrecioHistoricoForm(forms.ModelForm):

    # def __init__(self,*args, **kwargs):
    #     super(PrecioHistoricoForm, self).__init__(*args, **kwargs)
    #     self.fields['margen'].validators = [valid_marge_range]

    class Meta:
        model = PrecioHistorico
        labels = {
            'fechaini': '',
            'fechafin': '',
            'costo93': '',
            'iva93': '',
            'impfijo93': '',
            'impvar93': '',
            'imptotal93': '',
            'oc93': '',
            'costo95': '',
            'iva95': '',
            'impfijo95': '',
            'impvar95': '',
            'imptotal95': '',
            'oc95': '',
            'costo97': '',
            'iva97': '',
            'impfijo97': '',
            'impvar97': '',
            'imptotal97': '',
            'oc97': '',
            'costodiesel': '',
            'ivadiesel': '',
            'impfijodiesel': '',
            'impvardiesel': '',
            'imptotaldiesel': '',
            'diesel': '',
            'margen': '',
        }
        fields = [
            'fechaini',
            'fechafin',

            'costo93',
            'iva93',
            'impfijo93',
            'impvar93',
            'imptotal93',
            'oc93',

            'costo95',
            'iva95',
            'impfijo95',
            'impvar95',
            'imptotal95',
            'oc95',

            'costo97',
            'iva97',
            'impfijo97',
            'impvar97',
            'imptotal97',
            'oc97',

            'costodiesel',
            'ivadiesel',
            'impfijodiesel',
            'impvardiesel',
            'imptotaldiesel',
            'diesel',

            'margen',

        ]

        widgets = {
            'margen': forms.NumberInput(attrs={'class': 'form-control','tabindex':'2','minlength':'1','maxlength':'2'
                , 'placeholder': 'Ganancia'}),
            'fechaini': DateWidget(attrs={'id': "fechaini",'tabindex':'1'}, usel10n=True, bootstrap_version=3),
            'fechafin':DateWidget(attrs={'id': "fechafin"}, usel10n=True, bootstrap_version=3),

            'costo93': forms.NumberInput(attrs={'class': 'form-control','tabindex':'3', 'placeholder': 'Ej: 300, 400'}),
            'iva93': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'3'}),
            'impfijo93':forms.NumberInput(attrs={'class': 'form-control','tabindex':'4', 'placeholder': 'Ej: 1,1, -0,1'}),
            'impvar93':forms.NumberInput(attrs={'class': 'form-control','tabindex':'5', 'placeholder': 'Ej: 1,1, -0,1'}),
            'imptotal93':forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'6'}),
            'oc93': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'7'}),

            'costo95': forms.NumberInput(attrs={'class': 'form-control','tabindex':'8', 'placeholder': 'Ej: 300, 400'}),
            'iva95': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'9'}),
            'impfijo95': forms.NumberInput(attrs={'class': 'form-control','tabindex':'10', 'placeholder': 'Ej: 1,1, -0,1'}),
            'impvar95': forms.NumberInput(attrs={'class': 'form-control','tabindex':'11', 'placeholder': 'Ej: 1,1, -0,1'}),
            'imptotal95': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'12'}),
            'oc95': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'13'}),

            'costo97': forms.NumberInput(attrs={'class': 'form-control','tabindex':'14', 'placeholder': 'Ej: 300, 400'}),
            'iva97': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'15'}),
            'impfijo97': forms.NumberInput(attrs={'class': 'form-control','tabindex':'16', 'placeholder': 'Ej: 1,1, -0,1'}),
            'impvar97': forms.NumberInput(attrs={'class': 'form-control','tabindex':'17', 'placeholder': 'Ej: 1,1, -0,1'}),
            'imptotal97': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'18'}),
            'oc97': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'19'}),

            'costodiesel': forms.NumberInput(attrs={'class': 'form-control','tabindex':'20', 'placeholder': 'Ej: 300, 400'}),
            'ivadiesel': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'21'}),
            'impfijodiesel': forms.NumberInput(attrs={'class': 'form-control','tabindex':'22', 'placeholder': 'Ej: 1,1, -0,1'}),
            'impvardiesel': forms.NumberInput(attrs={'class': 'form-control','tabindex':'23', 'placeholder': 'Ej: 1,1, -0,1'}),
            'imptotaldiesel': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'24'}),
            'diesel': forms.NumberInput(attrs={'class': 'form-control','readonly':'readonly','tabindex':'25'}),


        }
    def clean_fechaini(self):
        fecha = datetime.today()
        max = timedelta(days=7)
        min = timedelta(days=-7)
        fechamin = fecha + min
        fechamax = fecha + max
        print(fechamin)
        print(fechamax)
        fecha_emision = self.cleaned_data['fechaini']
        if fecha_emision > fechamax.date() or fecha_emision < fechamin.date():
            raise forms.ValidationError('Fecha fuera de rango')
        else:
            return self.cleaned_data['fechaini']
import django_filters
class SearchPrecioForm(FilterSet):
    fechaini = django_filters.DateFilter( 'fechaini', label=('Fecha Inicio'),
        widget=AdminDateWidget())
    fechafin = django_filters.DateFilter('fechafin', label=('Fecha Fin'),
        widget=AdminDateWidget())
    class Meta:
        model = PrecioHistorico
        fields = {
            'fechaini': ['exact'],
            'fechafin': ['exact'],
        }
