from itertools import cycle

import sys
from django import forms
from .models import *
from datetimewidget.widgets import DateWidget
from datetime import datetime,timedelta
from django_filters import FilterSet
import django_filters
class  HandlingClienteForm(forms.ModelForm):
    class Meta:
        model = HandlingCliente
        fields = [
            'nombre',
            'rut',
            'telefono',
        ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre cliente'}),
            'rut': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej: 123456789'}),
            'telefono': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '+56912345678'}),

        }

    def clean_rut(self):
        rut = self.cleaned_data['rut']
        rut = rut.upper();
        rut = rut.replace("-", "")
        rut = rut.replace(".", "")
        aux = rut[:-1]
        dv = rut[-1:]

        revertido = map(int, reversed(str(aux)))
        factors = cycle(range(2, 8))
        s = sum(d * f for d, f in zip(revertido, factors))
        res = (-s) % 11

        if str(res) == dv:
            return rut
        elif dv == "K" and res == 10:
            return rut
        else:
            raise forms.ValidationError('RUT incorrecto')

class  HandlingDiarioForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(HandlingDiarioForm, self).__init__(*args,**kwargs)
        self.fields['handlingcliente'].queryset = HandlingCliente.objects.exclude(estado='Inactivo').order_by('-id')
    class Meta:
        model = HandlingDiario
        fields = [
            'fecha',
            'kilometraje',
            'patente',
            'nombre_chofer',
            'litros',
            'handlingcliente',
        ]
        widgets = {
            'fecha': DateWidget(attrs={'id': "fecha"}, usel10n=True, bootstrap_version=3),
            'kilometraje': forms.NumberInput(attrs={'class': 'form-control'}),
            'patente': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej: KKCK77'}),
            'nombre_chofer': forms.TextInput(attrs={'class': 'form-control'}),
            'litros': forms.NumberInput(attrs={'class': 'form-control'}),
            'n_guia_entrega': forms.TextInput(attrs={'class': 'form-control'}),
            'handlingcliente': forms.Select(attrs={'class': 'form-control'}),

        }
    def clean_fecha(self):
        fecha = datetime.today()
        min = timedelta(days=-1)
        fechamin = fecha + min
        fechamax = fecha
        print(fechamin)
        print(fechamax)
        fecha_emision = self.cleaned_data['fecha']
        if fecha_emision > fechamax.date() or fecha_emision < fechamin.date():
            raise forms.ValidationError('Fecha fuera de rango')
        else:
            return self.cleaned_data['fecha']

class  HandlingIngresoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(HandlingIngresoForm, self).__init__(*args, **kwargs)
        self.fields['handlingcliente'].queryset = HandlingCliente.objects.exclude(estado='Inactivo').order_by('-id')
    class Meta:
        model = IngresoHandling
        fields = [
            'fecha',
            'numero_guia',
            'recepcion',
            'handlingcliente',
        ]
        widgets = {
            'fecha': DateWidget(attrs={'id': "fecha"}, usel10n=True, bootstrap_version=3),
            'numero_guia': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ej: 1234'}),
            'recepcion': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Ej: 500, ingresar valores enteros'}),
            'handlingcliente': forms.Select(attrs={'class': 'form-control'}),
        }

    def clean_fecha(self):
        fecha = datetime.today()
        max = timedelta(days=-3)
        fechamin = fecha + max
        fechamax = fecha
        print(fechamin)
        print(fechamax)
        fecha_descarga = self.cleaned_data['fecha']
        if fecha_descarga > fechamax.date() or fecha_descarga < fechamin.date():
            raise forms.ValidationError('Fecha fuera de rango')
        else:
            return self.cleaned_data['fecha']
class ClienteDeleteForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(ClienteDeleteForm, self).__init__(*args,**kwargs)
        self.initial['estado']='Inactivo'

    class Meta:
        model = HandlingCliente

        fields = [
            'estado'
        ]
        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control','style':'visibility:hidden;'}),
        }
class ClienteDeleteFormin(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(ClienteDeleteFormin, self).__init__(*args,**kwargs)
        self.initial['estado']='Activo'

    class Meta:
        model = HandlingCliente

        fields = [
            'estado'
        ]
        widgets = {
            'estado': forms.Select(attrs={'class': 'form-control','style':'visibility:hidden;'}),
        }
class SearchClienteForm(FilterSet):
    rut = django_filters.Filter('rut', label=('Rut'))
    nombre = django_filters.ModelMultipleChoiceFilter(queryset = HandlingCliente.objects.exclude(estado='Inactivo').order_by('-id'),
                                                      widget=forms.SelectMultiple)

    class Meta:
        model = HandlingCliente
        fields = {
            'rut',
            'nombre'
        }
class SearchRetiroForm(FilterSet):
    handlingcliente = django_filters.ModelMultipleChoiceFilter(queryset = HandlingCliente.objects.exclude(estado='Inactivo').order_by('-id'),
                                                      widget=forms.SelectMultiple)
    class Meta:
        model = HandlingDiario
        fields = {
            'handlingcliente': ['exact'],
            'fecha': ['exact'],
        }
class SearchIngresoForm(FilterSet):
    handlingcliente = django_filters.ModelMultipleChoiceFilter(queryset = HandlingCliente.objects.exclude(estado='Inactivo').order_by('-id'),
                                                      widget=forms.Select, label=('Cliente'))
    fecha = django_filters.DateFilter('fecha', label=('Fecha'))
    class Meta:
        model = IngresoHandling
        fields = {
            'handlingcliente',
            'fecha',
        }