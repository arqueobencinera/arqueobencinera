from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django_tables2 import SingleTableView
from .models import *
from .forms import *
from .table import *
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from users.mixins import PermissionsRequiredMixin
from django_filters.views import FilterView
from django.views.generic import View


class HandlingClienteCreate(LoginRequiredMixin,PermissionsRequiredMixin,SuccessMessageMixin,CreateView):
    model = HandlingCliente
    form_class = HandlingClienteForm
    required_permissions = ('handling.add_handlingcliente',)
    template_name = 'handling/handlingCliente_create.html'
    success_url = reverse_lazy('handling:handlingCliente_list')
    success_message = 'Exito'


class HandlingClienteUpdate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = HandlingCliente
    form_class = HandlingClienteForm
    required_permissions = ('handling.change_handlingcliente',)
    template_name = 'handling/handlingCliente_update.html'
    success_url = reverse_lazy('handling:handlingCliente_list')


class HandlingClienteDelete(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = HandlingCliente
    form_class = ClienteDeleteForm
    required_permissions = ('handling.delete_handlingcliente',)
    template_name = 'handling/handlingCliente_delete.html'
    success_url = reverse_lazy('handling:handlingCliente_list')

class HandlingClienteDeletein(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = HandlingCliente
    form_class = ClienteDeleteFormin
    required_permissions = ('handling.delete_handlingcliente',)
    template_name = 'handling/handlingCliente_deletein.html'
    success_url = reverse_lazy('handling:handlingCliente_listin')


class HandlingClienteList(FilterView,SingleTableView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    template_name = 'handling/list.html'
    model = HandlingCliente
    table_class = HandlingTable
    filterset_class = SearchClienteForm
    paginate_by = 20
    def get_queryset(self):
        qs1 = HandlingCliente.objects.exclude(estado='Inactivo')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return HandlingCliente.objects.filter(qs).filter(id__in=qs1).order_by('-id')
        return HandlingCliente.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(HandlingClienteList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context
class HandlingClienteListin(FilterView, SingleTableView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    template_name = 'handling/list.html'
    model = HandlingCliente
    table_class = HandlingTablein
    filterset_class = SearchClienteForm
    paginate_by = 20
    def get_queryset(self):
        qs1 = HandlingCliente.objects.exclude(estado='Activo')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return HandlingCliente.objects.filter(qs).filter(id__in=qs1).order_by('-id')

        return HandlingCliente.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(HandlingClienteListin, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context

class HandlingDiarioCreate(FilterView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,CreateView):
    model = HandlingDiario
    form_class = HandlingDiarioForm

    required_permissions = ('handling.add_handlingdiario',)
    template_name = 'handling/handlingDiario_create.html'
    success_url = reverse_lazy('handling:handlingDiario_list')
    success_message = 'Exito'


class HandlingDiarioUpdate( LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = HandlingDiario
    form_class = HandlingDiarioForm
    required_permissions = ('handling.change_handlingdiario',)
    template_name = 'handling/handlingDiario_update.html'
    success_url = reverse_lazy('handling:handlingDiario_lista')


class HandlingDiarioDelete(DeleteView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = HandlingDiario
    required_permissions = ('handling.delete_handlingdiario',)
    template_name = 'handling/handlingDiario_delete.html'
    success_url = reverse_lazy('handling:handlingDiario_lista')


class HandlingDiarioList(FilterView, LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,SingleTableView):
    template_name = 'handling/handlindiariolist.html'
    model = HandlingDiario
    filterset_class = SearchRetiroForm
    table_class = HandlingDiarioTable
    paginate_by = 20
    def get_queryset(self):
        qs1 = HandlingDiario.objects.exclude(handlingcliente__estado='Inactivo')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return HandlingDiario.objects.filter(qs).filter(id__in=qs1).order_by('-id')

        return HandlingDiario.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(HandlingDiarioList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context

#####################################################################################3
class HandlingIngresoCreate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,CreateView):
    model = IngresoHandling
    form_class = HandlingIngresoForm
    required_permissions = ('handling.add_ingresohandling',)
    template_name = 'handling/ingresohandling_create.html'
    success_url = reverse_lazy('handling:ingresohandling_list')
    success_message = 'Exito'


class HandlingIngresoUpdate(LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,UpdateView):
    model = IngresoHandling
    form_class = HandlingIngresoForm
    required_permissions = ('handling.change_ingresohandling',)
    template_name = 'handling/ingresohandling_update.html'
    success_url = reverse_lazy('handling:ingresohandling_list')


class HandlingIngresoDelete(DeleteView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin):
    model = IngresoHandling
    required_permissions = ('handling.delete_ingresohandling',)
    template_name = 'handling/ingresohandling_delete.html'
    success_url = reverse_lazy('handling:ingresohandling_list')


class HandlingIngresoList(FilterView,LoginRequiredMixin,SuccessMessageMixin,PermissionsRequiredMixin,SingleTableView):
    template_name = 'handling/ingresohandling_list.html'
    model = IngresoHandling
    filterset_class = SearchRetiroForm
    table_class = HandlingIngresoTable
    paginate_by = 20

    def get_queryset(self):
        qs1 = IngresoHandling.objects.exclude(handlingcliente__estado='Inactivo')
        #queryset1 = (Venta.objects.filter(id__in=qs1))
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = 0
            return IngresoHandling.objects.filter(qs).filter(id__in=qs1).order_by('-id')

        return IngresoHandling.objects.get_queryset().order_by('-id').filter(id__in=qs1)

    def get_context_data(self, **kwargs):
        context = super(HandlingIngresoList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context

