from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from combustibles.models import *
import datetime
# Create your models here.


class HandlingCliente(models.Model):
    nombre = models.CharField(max_length=50)
    rut = models.CharField(max_length=12)
    telefono = models.CharField(max_length=12)
    stock_litros = models.DecimalField(max_digits=8, decimal_places=3, default=0)
    estado = models.CharField(max_length=8, choices=(('Activo', 'Activo'), ('Inactivo', 'Inactivo')), default='Activo')
    def __str__(self):
        return self.nombre


class HandlingDiario(models.Model):
    fecha = models.DateField(default=datetime.date.today)
    kilometraje = models.IntegerField(validators=[MaxValueValidator(999999), MinValueValidator(0)])
    patente = models.CharField(max_length=6)
    nombre_chofer = models.CharField(max_length=50)
    litros = models.DecimalField(default=0, max_digits=8, decimal_places=3, validators=[MaxValueValidator(500), MinValueValidator(0)])
    handlingcliente = models.ForeignKey(HandlingCliente, verbose_name="Cliente Handling")


class IngresoHandling(models.Model):
    fecha = models.DateField(default=datetime.date.today)
    numero_guia = models.IntegerField(validators=[MaxValueValidator(99999999), MinValueValidator(0)])
    recepcion = models.DecimalField(max_digits=8, decimal_places=3, verbose_name="Litros Ingresados", validators=[MaxValueValidator(5000), MinValueValidator(0)])
    handlingcliente = models.ForeignKey(HandlingCliente, verbose_name="Cliente Handling")