from .models import *
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
import django_tables2 as tables

class HandlingTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = HandlingCliente
        template = 'django_tables2/bootstrap.html'
        exclude = ('id')
        fields = ('rut', 'nombre', 'telefono','stock_litros' )
    def __init__(self, *args, **kwargs):
        super(HandlingTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar')
        self.delete_text = _('Eliminar')


    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="far fa-edit"></i> {1}
            </a>
            <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{3}">
                <i class="fa fa-trash"></i> {3}
            </a>
            '''.format(

                reverse_lazy('handling:handlingCliente_editar', kwargs={'pk': record.pk}),
                'Modificar',
                reverse_lazy('handling:handlingCliente_eliminar', kwargs={'pk': record.pk}),
                'Desactivar'
            )
        )
class HandlingTablein(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = HandlingCliente
        template = 'django_tables2/bootstrap.html'
        exclude = ('id')
        fields = ('rut', 'nombre', 'telefono','stock_litros' )
    def __init__(self, *args, **kwargs):
        super(HandlingTablein, self).__init__(*args, **kwargs)
        self.delete_text = _('Eliminar')


    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{1}">
                <i class="fa fa-trash"></i> {1}
            </a>
            '''.format(
                reverse_lazy('handling:handlingCliente_eliminarin', kwargs={'pk': record.pk}),
                'Activar'
            )
        )
class HandlingDiarioTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = HandlingCliente
        template = 'django_tables2/bootstrap.html'
        exclude = ('id')
        fields = ('handlingcliente','fecha','litros')
    def __init__(self, *args, **kwargs):
        super(HandlingDiarioTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar')
        self.delete_text = _('Eliminar')


    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="far fa-edit"></i> {1}
            </a>
            <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{3}">
                <i class="fa fa-trash"></i> {3}
            </a>
            '''.format(

                reverse_lazy('handling:handlingDiario_editar', kwargs={'pk': record.pk}),
                'Modificar',
                reverse_lazy('handling:handlingDiario_eliminar', kwargs={'pk': record.pk}),
                'Eliminar'
            )
        )
class HandlingIngresoTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={'td': {'class': 'text-center'},'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = HandlingCliente
        template = 'django_tables2/bootstrap.html'
        exclude = ('id')
        fields = ('handlingcliente','fecha','numero_guia','recepcion')
    def __init__(self, *args, **kwargs):
        super(HandlingIngresoTable, self).__init__(*args, **kwargs)
        self.edit_text = _('Modificar')
        self.delete_text = _('Eliminar')


    def render_options(self, value, record):
        return mark_safe(
            '''
            <a href="{0}" class="btn btn-sm btn-success tooltip-link"
                data-original-title="{1}">
                <i class="far fa-edit"></i> {1}
            </a>
            <a href="{2}" class="btn btn-sm btn-danger tooltip-link"
                data-original-title="{3}">
                <i class="fa fa-trash"></i> {3}
            </a>
            '''.format(

                reverse_lazy('handling:ingresohandling_editar', kwargs={'pk': record.pk}),
                'Modificar',
                reverse_lazy('handling:ingresohandling_eliminar', kwargs={'pk': record.pk}),
                'Eliminar'
            )
        )