# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-11-26 18:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('handling', '0009_handlingdiario_saldo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='handlingdiario',
            name='saldo',
        ),
    ]
