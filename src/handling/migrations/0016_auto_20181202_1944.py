# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-12-02 22:44
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('handling', '0015_auto_20181130_2213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='handlingdiario',
            name='kilometraje',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(999999), django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='handlingdiario',
            name='litros',
            field=models.DecimalField(decimal_places=3, default=0, max_digits=8, validators=[django.core.validators.MaxValueValidator(500), django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='ingresohandling',
            name='numero_guia',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(99999999), django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='ingresohandling',
            name='recepcion',
            field=models.DecimalField(decimal_places=3, max_digits=8, validators=[django.core.validators.MaxValueValidator(5000), django.core.validators.MinValueValidator(0)], verbose_name='Litros Ingresados'),
        ),
    ]
