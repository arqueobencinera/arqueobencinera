from django.conf.urls import url, include
from .models import *
from .views import *
from .models import *

urlpatterns = [

    # CRUD Ingreso de Handling Cliente
    url(r'^handlingcliente/crear', HandlingClienteCreate.as_view(), name='handlingCliente_crear'),
    url(r'^handlingcliente/listar/activa', HandlingClienteList.as_view(), name='handlingCliente_list'),
    url(r'^handlingcliente/listar/inactiva', HandlingClienteListin.as_view(), name='handlingCliente_listin'),
    url(r'^handlingcliente/editar/(?P<pk>\d+)$', HandlingClienteUpdate.as_view(), name='handlingCliente_editar'),
    url(r'^handlingcliente/desactivar/(?P<pk>\d+)$', HandlingClienteDelete.as_view(), name='handlingCliente_eliminar'),
    url(r'^handlingcliente/activar/(?P<pk>\d+)$', HandlingClienteDeletein.as_view(), name='handlingCliente_eliminarin'),
    # CRUD Ingreso de Handling Diario
    url(r'^handlingdiario/crear', HandlingDiarioCreate.as_view(), name='handlingDiario_crear'),
    url(r'^handlingdiario/listar', HandlingDiarioList.as_view(), name='handlingDiario_list'),
    url(r'^handlingdiario/editar/(?P<pk>\d+)$', HandlingDiarioUpdate.as_view(), name='handlingDiario_editar'),
    url(r'^handlingdiario/eliminar/(?P<pk>\d+)$', HandlingDiarioDelete.as_view(), name='handlingDiario_eliminar'),
    # CRUD Ingreso de Detalle Handling
     url(r'^IngresoHandling/crear', HandlingIngresoCreate.as_view(), name='ingresohandling_crear'),
     url(r'^IngresoHandling/editar/(?P<pk>\d+)$', HandlingIngresoUpdate.as_view(), name='ingresohandling_editar'),
     url(r'^IngresoHandling/eliminar/(?P<pk>\d+)$', HandlingIngresoDelete.as_view(), name='ingresohandling_eliminar'),
     url(r'^IngresoHandling/listar', HandlingIngresoList.as_view(), name='ingresohandling_list'),
]