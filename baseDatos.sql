PGDMP     6    ;                v           reco    9.6.6    9.6.6 �    �	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �	           1262    76787    reco    DATABASE     �   CREATE DATABASE reco WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE reco;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    77337    addid()    FUNCTION     f  CREATE FUNCTION addid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare 
    id1 int;
    id2 int;
    
    BEGIN
    	
    	id1 = (select max(id)from venta_venta);
        id2 = (select max(id) from venta_arqueo);
        update venta_arqueo set venta_id = id1 where id = cast(id2 as integer);
        
        
        return null;
    END;
$$;
    DROP FUNCTION public.addid();
       public       postgres    false    3    1            �            1255    77327    agreagarcombustible()    FUNCTION     �  CREATE FUNCTION agreagarcombustible() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare 
    id1 int;
    litros93 int;
    litro97 int;
    litrodiesel int;
    var93 int;
    var97 int;
    vardiesel int;
    suma93 int;
    suma97 int;
    sumadiesel int;
    BEGIN
    	
    	id1 = (select max(id)from compra_compra);
        litros93 = (select oc93 from compra_compra where id = id1 );
        litro97 = (select oc97 from compra_compra where id = id1 );
        litrodiesel = (select diesel from compra_compra where id = id1 );
        
        var93 = (select stock from combustibles_combustible where id = 1);
        var97 = (select stock from combustibles_combustible where id = 3);
        vardiesel = (select stock from combustibles_combustible where id = 4);
        
        suma93 = litros93 + var93;
        suma97 = litro97 + var97;
        sumadiesel = litrodiesel + vardiesel;
        update combustibles_combustible set stock = cast(suma93 as int) where id = 1 ;
        update combustibles_combustible set stock = cast(suma97 as int) where id = 3 ;
        update combustibles_combustible set stock = cast(sumadiesel as int) where id = 4 ;
        return null;
    END;
$$;
 ,   DROP FUNCTION public.agreagarcombustible();
       public       postgres    false    1    3            �            1255    77329    eliminarcompra()    FUNCTION     g  CREATE FUNCTION eliminarcompra() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare
    id10 int;
    id11 int;
    valor1 int;
    oc1 int;
    total1 int;
    valor2 int;
    oc2 int;
    total2 int;
    valor3 int;
    oc3 int;
    total3 int;
    BEGIN
    	 id10 = old.id;
         id11 = (select id from compra_compra where id = id10);
         if EXISTS (select id from compra_compra where id = id10) THEN
         	valor1 =(select oc93 from compra_compra where id = id11);
            oc1 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =1);
            total1 = oc1 -valor1;
              update combustibles_combustible set stock = cast(total1 as int) where id = 1 ;
         end if;
         
         
         
         if EXISTS (select id from compra_compra where id = id10) THEN
         	valor2 =(select oc97 from compra_compra where id = id11);
            oc2 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =3);
            total2 = oc2 -valor2;
              update combustibles_combustible set stock = cast(total2 as int) where id = 3 ;
         end if;
         
         
         if EXISTS (select id from compra_compra where id = id10) THEN
         	valor3 =(select diesel from compra_compra where id = id11);
            oc3 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =4);
            total3 = oc3 -valor3;
              update combustibles_combustible set stock = cast(total3 as int) where id = 4 ;
         end if;
        return null;
    END;
$$;
 '   DROP FUNCTION public.eliminarcompra();
       public       postgres    false    3    1            �            1255    77339    eliminarventa()    FUNCTION     �  CREATE FUNCTION eliminarventa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare
    id10 int;
    id11 int;
    id12 int;
    id13 int;
    id14 int;
    valor1 int;
    oc1 int;
    total1 int;
    valor2 int;
    oc2a int;
    oc2b int;
    total2a int;
    total2b int;
    valor3 int;
    oc3 int;
    total3 int;
    valor4 int;
    oc4 int;
    total4 int;
    BEGIN
    	id10 = old.id;
         id11 = (select id from venta_detalleventa where venta_id = id10 and combustible_id =1);
         id12 = (select id from venta_detalleventa where venta_id = id10 and combustible_id =2); 
         id13 = (select id from venta_detalleventa where venta_id = id10 and combustible_id =3); 
         id14 = (select id from venta_detalleventa where venta_id = id10 and combustible_id =4);
         if EXISTS (select id from venta_detalleventa where venta_id = id10 and combustible_id =1) THEN
         	valor1 =(select tot_venta_litro from venta_detalleventa where id = id11);
            oc1 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =1);
            total1 = oc1 +valor1;
              update combustibles_combustible set stock = cast(total1 as int) where id = 1 ;
         end if;
         
         
         if EXISTS (select id from venta_detalleventa where venta_id = id10 and combustible_id =2) THEN
         	valor2 =(select tot_venta_litro from venta_detalleventa where id = id12);
            oc2a =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =1);
            oc2b =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =3);
            total2a = (oc2a +(valor2/2));
            total2b = (oc2b +(valor2/2));
              update combustibles_combustible set stock = cast(total2a as int) where id = 1 ;
              update combustibles_combustible set stock = cast(total2b as int) where id = 3 ;
         end if;
         
         
         if EXISTS (select id from venta_detalleventa where venta_id = id10 and combustible_id =3) THEN
         	valor3 =(select tot_venta_litro from venta_detalleventa where id = id13);
            oc3 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =3);
            total3 = oc3 +valor3;
              update combustibles_combustible set stock = cast(total3 as int) where id = 3 ;
         end if;
         
         
         if EXISTS (select id from venta_detalleventa where venta_id = id10 and combustible_id =4) THEN
         	valor4 =(select tot_venta_litro from venta_detalleventa where id = id14);
            oc4 =(select combustibles_combustible.stock from combustibles_combustible 
                  where id =4);
            total4 = oc4 +valor4;
              update combustibles_combustible set stock = cast(total4 as int) where id = 4 ;
         end if;
         
        return null;
    END;
$$;
 &   DROP FUNCTION public.eliminarventa();
       public       postgres    false    3    1            �            1255    77331    handlingdescuento()    FUNCTION     �  CREATE FUNCTION handlingdescuento() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare 
    id1 int;
    cliente int;
    litrosres int;
    stockcliente int ;
    stockcombustible int;
    resta int;
    
    BEGIN
    	
    	id1 = (select max(id)from handling_handlingdiario);
        litrosres = (select litros from handling_handlingdiario where id = id1);
        
        cliente = (select handlingcliente_id  from handling_handlingdiario  where id = id1);
        
        stockcliente = (select stock_litros from handling_handlingcliente where id = cliente);
        stockcombustible = (select combustibles_combustible.stock from combustibles_combustible where id = 4);
     
        resta = stockcombustible - litrosres;
        update combustibles_combustible set stock = cast(resta as int) where id = 4 ;
        resta = stockcliente - litrosres;
        update handling_handlingcliente set stock_litros = cast(resta as int) where id = cliente ;

        
        return null;
    END;
$$;
 *   DROP FUNCTION public.handlingdescuento();
       public       postgres    false    1    3            �            1255    77333    handlingingreso()    FUNCTION     
  CREATE FUNCTION handlingingreso() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare 
    id1 int;
    cliente int;
    litrosres int;
    stockcliente int ;
    stockcombustible int;
    resta int;
    
    BEGIN
    	
    	id1 = (select max(id)from handling_ingresohandling);
        litrosres = (select recepcion from handling_ingresohandling where id = id1);
        
        cliente = (select handlingcliente_id  from handling_ingresohandling  where id = id1);
        
        stockcliente = (select stock_litros from handling_handlingcliente where id = cliente);
        
     
        resta = stockcliente + litrosres;
        update handling_handlingcliente set stock_litros = cast(resta as int) where id = cliente ;

        
        return null;
    END;
$$;
 (   DROP FUNCTION public.handlingingreso();
       public       postgres    false    1    3            �            1255    77335    quitarcombustible()    FUNCTION     j  CREATE FUNCTION quitarcombustible() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    declare 
    id1 int;
    oc int;
    des int ;
    stock int;
    resta int;
    stock93 int;
    stock97 int;
    resta1 int;
    resta2 int;
    
    BEGIN
    	
    	id1 = (select max(id)from venta_detalleventa);
        
        oc = (select combustible_id  from venta_detalleventa  where id = id1);
        
        des = (select tot_venta_litro from venta_detalleventa where id = id1);
        stock = (select combustibles_combustible.stock from combustibles_combustible where id = oc);
        
        if oc = 1 THEN
        resta = stock - des;
            update combustibles_combustible set stock = cast(resta as int) where id = 1 ;
        END IF;
        
        if oc = 2 THEN
         	stock93 = (select combustibles_combustible.stock from combustibles_combustible where id = 1);
            stock97 = (select combustibles_combustible.stock from combustibles_combustible where id = 3);
        	resta1 = (stock93 - (des/2));
            resta2 = (stock97 - (des/2));
            update combustibles_combustible set stock = cast(resta1 as int) where id = 1 ;
           	update combustibles_combustible set stock = cast(resta2 as int) where id = 3 ;
        END IF;
        
         if oc = 3 THEN
         	resta = stock - des;
            update combustibles_combustible set stock = cast(resta as int) where id = 3 ;
        END IF;
        if oc = 4 THEN
        	resta = stock - des;	
            update combustibles_combustible set stock = cast(resta as int) where id = 4 ;
        END IF;
        
        return null;
    END;
$$;
 *   DROP FUNCTION public.quitarcombustible();
       public       postgres    false    3    1            �            1259    76915    account_emailaddress    TABLE     �   CREATE TABLE account_emailaddress (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);
 (   DROP TABLE public.account_emailaddress;
       public         postgres    false    3            �            1259    76913    account_emailaddress_id_seq    SEQUENCE     }   CREATE SEQUENCE account_emailaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.account_emailaddress_id_seq;
       public       postgres    false    202    3            �	           0    0    account_emailaddress_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE account_emailaddress_id_seq OWNED BY account_emailaddress.id;
            public       postgres    false    201            �            1259    76925    account_emailconfirmation    TABLE     �   CREATE TABLE account_emailconfirmation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id integer NOT NULL
);
 -   DROP TABLE public.account_emailconfirmation;
       public         postgres    false    3            �            1259    76923     account_emailconfirmation_id_seq    SEQUENCE     �   CREATE SEQUENCE account_emailconfirmation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.account_emailconfirmation_id_seq;
       public       postgres    false    204    3            �	           0    0     account_emailconfirmation_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE account_emailconfirmation_id_seq OWNED BY account_emailconfirmation.id;
            public       postgres    false    203            �            1259    76819 
   auth_group    TABLE     �   CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    description character varying(180)
);
    DROP TABLE public.auth_group;
       public         postgres    false    3            �            1259    76817    auth_group_id_seq    SEQUENCE     s   CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public       postgres    false    3    192            �	           0    0    auth_group_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;
            public       postgres    false    191            �            1259    76829    auth_group_permissions    TABLE     �   CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         postgres    false    3            �            1259    76827    auth_group_permissions_id_seq    SEQUENCE        CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public       postgres    false    194    3            �	           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;
            public       postgres    false    193            �            1259    76811    auth_permission    TABLE     �   CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         postgres    false    3            �            1259    76809    auth_permission_id_seq    SEQUENCE     x   CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public       postgres    false    190    3            �	           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;
            public       postgres    false    189            �            1259    76837 	   auth_user    TABLE     �  CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         postgres    false    3            �            1259    76847    auth_user_groups    TABLE     x   CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         postgres    false    3            �            1259    76845    auth_user_groups_id_seq    SEQUENCE     y   CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public       postgres    false    3    198            �	           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;
            public       postgres    false    197            �            1259    76835    auth_user_id_seq    SEQUENCE     r   CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public       postgres    false    196    3            �	           0    0    auth_user_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;
            public       postgres    false    195            �            1259    76855    auth_user_user_permissions    TABLE     �   CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         postgres    false    3            �            1259    76853 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public       postgres    false    200    3            �	           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;
            public       postgres    false    199            �            1259    76997    combustibles_combustible    TABLE     �   CREATE TABLE combustibles_combustible (
    id integer NOT NULL,
    octanaje character varying(10),
    stock integer NOT NULL,
    stock_minimo integer NOT NULL
);
 ,   DROP TABLE public.combustibles_combustible;
       public         postgres    false    3            �            1259    76995    combustibles_combustible_id_seq    SEQUENCE     �   CREATE SEQUENCE combustibles_combustible_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.combustibles_combustible_id_seq;
       public       postgres    false    3    208            �	           0    0    combustibles_combustible_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE combustibles_combustible_id_seq OWNED BY combustibles_combustible.id;
            public       postgres    false    207            �            1259    77075    compra_compra    TABLE     u  CREATE TABLE compra_compra (
    id integer NOT NULL,
    fecha_emision date NOT NULL,
    fecha_descarga date NOT NULL,
    patente character varying(6) NOT NULL,
    nombre_chofer character varying(25) NOT NULL,
    oc93 integer NOT NULL,
    oc97 integer NOT NULL,
    diesel integer NOT NULL,
    precio_id integer NOT NULL,
    estado character varying(8) NOT NULL
);
 !   DROP TABLE public.compra_compra;
       public         postgres    false    3            �            1259    77073    compra_compra_id_seq    SEQUENCE     v   CREATE SEQUENCE compra_compra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.compra_compra_id_seq;
       public       postgres    false    3    212            �	           0    0    compra_compra_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE compra_compra_id_seq OWNED BY compra_compra.id;
            public       postgres    false    211            �            1259    76957    django_admin_log    TABLE     �  CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         postgres    false    3            �            1259    76955    django_admin_log_id_seq    SEQUENCE     y   CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public       postgres    false    206    3            �	           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;
            public       postgres    false    205            �            1259    76801    django_content_type    TABLE     �   CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         postgres    false    3            �            1259    76799    django_content_type_id_seq    SEQUENCE     |   CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public       postgres    false    3    188            �	           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;
            public       postgres    false    187            �            1259    76790    django_migrations    TABLE     �   CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         postgres    false    3            �            1259    76788    django_migrations_id_seq    SEQUENCE     z   CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public       postgres    false    3    186            �	           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;
            public       postgres    false    185            �            1259    77243    django_session    TABLE     �   CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         postgres    false    3            �            1259    77255    django_site    TABLE     �   CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.django_site;
       public         postgres    false    3            �            1259    77253    django_site_id_seq    SEQUENCE     t   CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.django_site_id_seq;
       public       postgres    false    3    225            �	           0    0    django_site_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;
            public       postgres    false    224            �            1259    77112    handling_handlingcliente    TABLE     �   CREATE TABLE handling_handlingcliente (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    rut character varying(12) NOT NULL,
    telefono character varying(12) NOT NULL,
    stock_litros numeric(8,3) NOT NULL
);
 ,   DROP TABLE public.handling_handlingcliente;
       public         postgres    false    3            �            1259    77110    handling_handlingcliente_id_seq    SEQUENCE     �   CREATE SEQUENCE handling_handlingcliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.handling_handlingcliente_id_seq;
       public       postgres    false    214    3            �	           0    0    handling_handlingcliente_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE handling_handlingcliente_id_seq OWNED BY handling_handlingcliente.id;
            public       postgres    false    213            �            1259    77120    handling_handlingdiario    TABLE     %  CREATE TABLE handling_handlingdiario (
    id integer NOT NULL,
    fecha date NOT NULL,
    kilometraje integer NOT NULL,
    patente character varying(6) NOT NULL,
    nombre_chofer character varying(50) NOT NULL,
    litros numeric(8,3) NOT NULL,
    handlingcliente_id integer NOT NULL
);
 +   DROP TABLE public.handling_handlingdiario;
       public         postgres    false    3            �            1259    77118    handling_handlingdiario_id_seq    SEQUENCE     �   CREATE SEQUENCE handling_handlingdiario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.handling_handlingdiario_id_seq;
       public       postgres    false    3    216            �	           0    0    handling_handlingdiario_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE handling_handlingdiario_id_seq OWNED BY handling_handlingdiario.id;
            public       postgres    false    215            �            1259    77181    handling_ingresohandling    TABLE     �   CREATE TABLE handling_ingresohandling (
    id integer NOT NULL,
    fecha date NOT NULL,
    numero_guia integer NOT NULL,
    recepcion numeric(8,3) NOT NULL,
    handlingcliente_id integer NOT NULL
);
 ,   DROP TABLE public.handling_ingresohandling;
       public         postgres    false    3            �            1259    77179    handling_ingresohandling_id_seq    SEQUENCE     �   CREATE SEQUENCE handling_ingresohandling_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.handling_ingresohandling_id_seq;
       public       postgres    false    3    218            �	           0    0    handling_ingresohandling_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE handling_ingresohandling_id_seq OWNED BY handling_ingresohandling.id;
            public       postgres    false    217            �            1259    77005    precio_preciohistorico    TABLE     �  CREATE TABLE precio_preciohistorico (
    id integer NOT NULL,
    fechaini date,
    fechafin date,
    costo93 integer,
    iva93 numeric(8,4),
    impfijo93 numeric(8,3),
    impvar93 numeric(8,3),
    imptotal93 numeric(8,3),
    oc93 integer,
    costo95 integer,
    iva95 numeric(8,3),
    impfijo95 numeric(8,3),
    impvar95 numeric(8,3),
    imptotal95 numeric(8,3),
    oc95 integer,
    costo97 integer,
    iva97 numeric(8,3),
    impfijo97 numeric(8,3),
    impvar97 numeric(8,3),
    imptotal97 numeric(8,3),
    oc97 integer,
    costodiesel integer,
    ivadiesel numeric(8,3),
    impfijodiesel numeric(8,3),
    impvardiesel numeric(8,3),
    imptotaldiesel numeric(8,3),
    diesel integer,
    margen integer
);
 *   DROP TABLE public.precio_preciohistorico;
       public         postgres    false    3            �            1259    77003    precio_preciohistorico_id_seq    SEQUENCE        CREATE SEQUENCE precio_preciohistorico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.precio_preciohistorico_id_seq;
       public       postgres    false    3    210            �	           0    0    precio_preciohistorico_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE precio_preciohistorico_id_seq OWNED BY precio_preciohistorico.id;
            public       postgres    false    209            �            1259    77202    reversion_revision    TABLE     �   CREATE TABLE reversion_revision (
    id integer NOT NULL,
    date_created timestamp with time zone NOT NULL,
    comment text NOT NULL,
    user_id integer
);
 &   DROP TABLE public.reversion_revision;
       public         postgres    false    3            �            1259    77200    reversion_revision_id_seq    SEQUENCE     {   CREATE SEQUENCE reversion_revision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.reversion_revision_id_seq;
       public       postgres    false    3    220            �	           0    0    reversion_revision_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE reversion_revision_id_seq OWNED BY reversion_revision.id;
            public       postgres    false    219            �            1259    77213    reversion_version    TABLE     I  CREATE TABLE reversion_version (
    id integer NOT NULL,
    object_id character varying(191) NOT NULL,
    format character varying(255) NOT NULL,
    serialized_data text NOT NULL,
    object_repr text NOT NULL,
    content_type_id integer NOT NULL,
    revision_id integer NOT NULL,
    db character varying(191) NOT NULL
);
 %   DROP TABLE public.reversion_version;
       public         postgres    false    3            �            1259    77211    reversion_version_id_seq    SEQUENCE     z   CREATE SEQUENCE reversion_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.reversion_version_id_seq;
       public       postgres    false    3    222            �	           0    0    reversion_version_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE reversion_version_id_seq OWNED BY reversion_version.id;
            public       postgres    false    221            �            1259    77300    venta_arqueo    TABLE     8  CREATE TABLE venta_arqueo (
    id integer NOT NULL,
    tot_efectivo integer NOT NULL,
    cheque integer NOT NULL,
    credito integer NOT NULL,
    tarjecredebtbk integer NOT NULL,
    tarjedebtbk integer NOT NULL,
    subtotaltarje integer NOT NULL,
    tot_ingreso integer NOT NULL,
    venta_id integer
);
     DROP TABLE public.venta_arqueo;
       public         postgres    false    3            �            1259    77298    venta_arqueo_id_seq    SEQUENCE     u   CREATE SEQUENCE venta_arqueo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.venta_arqueo_id_seq;
       public       postgres    false    3    231            �	           0    0    venta_arqueo_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE venta_arqueo_id_seq OWNED BY venta_arqueo.id;
            public       postgres    false    230            �            1259    77266    venta_detalleventa    TABLE     �   CREATE TABLE venta_detalleventa (
    id integer NOT NULL,
    tot_venta_litro numeric(8,3) NOT NULL,
    combustible_id integer NOT NULL,
    venta_id integer NOT NULL
);
 &   DROP TABLE public.venta_detalleventa;
       public         postgres    false    3            �            1259    77264    venta_detalleventa_id_seq    SEQUENCE     {   CREATE SEQUENCE venta_detalleventa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.venta_detalleventa_id_seq;
       public       postgres    false    227    3            �	           0    0    venta_detalleventa_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE venta_detalleventa_id_seq OWNED BY venta_detalleventa.id;
            public       postgres    false    226            �            1259    77274    venta_venta    TABLE     �   CREATE TABLE venta_venta (
    id integer NOT NULL,
    fecha date NOT NULL,
    turno character varying(2) NOT NULL,
    precio_id integer NOT NULL,
    estado character varying(8) NOT NULL
);
    DROP TABLE public.venta_venta;
       public         postgres    false    3            �            1259    77272    venta_venta_id_seq    SEQUENCE     t   CREATE SEQUENCE venta_venta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.venta_venta_id_seq;
       public       postgres    false    229    3            �	           0    0    venta_venta_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE venta_venta_id_seq OWNED BY venta_venta.id;
            public       postgres    false    228            n           2604    76918    account_emailaddress id    DEFAULT     t   ALTER TABLE ONLY account_emailaddress ALTER COLUMN id SET DEFAULT nextval('account_emailaddress_id_seq'::regclass);
 F   ALTER TABLE public.account_emailaddress ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    201    202            o           2604    76928    account_emailconfirmation id    DEFAULT     ~   ALTER TABLE ONLY account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('account_emailconfirmation_id_seq'::regclass);
 K   ALTER TABLE public.account_emailconfirmation ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    204    204            i           2604    76822    auth_group id    DEFAULT     `   ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    191    192    192            j           2604    76832    auth_group_permissions id    DEFAULT     x   ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    193    194    194            h           2604    76814    auth_permission id    DEFAULT     j   ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    189    190    190            k           2604    76840    auth_user id    DEFAULT     ^   ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    195    196    196            l           2604    76850    auth_user_groups id    DEFAULT     l   ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    198    198            m           2604    76858    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    199    200            r           2604    77000    combustibles_combustible id    DEFAULT     |   ALTER TABLE ONLY combustibles_combustible ALTER COLUMN id SET DEFAULT nextval('combustibles_combustible_id_seq'::regclass);
 J   ALTER TABLE public.combustibles_combustible ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    208    208            t           2604    77078    compra_compra id    DEFAULT     f   ALTER TABLE ONLY compra_compra ALTER COLUMN id SET DEFAULT nextval('compra_compra_id_seq'::regclass);
 ?   ALTER TABLE public.compra_compra ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    212    211    212            p           2604    76960    django_admin_log id    DEFAULT     l   ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    206    206            g           2604    76804    django_content_type id    DEFAULT     r   ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    188    187    188            f           2604    76793    django_migrations id    DEFAULT     n   ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    185    186            z           2604    77258    django_site id    DEFAULT     b   ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);
 =   ALTER TABLE public.django_site ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    225    224    225            u           2604    77115    handling_handlingcliente id    DEFAULT     |   ALTER TABLE ONLY handling_handlingcliente ALTER COLUMN id SET DEFAULT nextval('handling_handlingcliente_id_seq'::regclass);
 J   ALTER TABLE public.handling_handlingcliente ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    213    214    214            v           2604    77123    handling_handlingdiario id    DEFAULT     z   ALTER TABLE ONLY handling_handlingdiario ALTER COLUMN id SET DEFAULT nextval('handling_handlingdiario_id_seq'::regclass);
 I   ALTER TABLE public.handling_handlingdiario ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    215    216    216            w           2604    77184    handling_ingresohandling id    DEFAULT     |   ALTER TABLE ONLY handling_ingresohandling ALTER COLUMN id SET DEFAULT nextval('handling_ingresohandling_id_seq'::regclass);
 J   ALTER TABLE public.handling_ingresohandling ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    218    218            s           2604    77008    precio_preciohistorico id    DEFAULT     x   ALTER TABLE ONLY precio_preciohistorico ALTER COLUMN id SET DEFAULT nextval('precio_preciohistorico_id_seq'::regclass);
 H   ALTER TABLE public.precio_preciohistorico ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    209    210    210            x           2604    77205    reversion_revision id    DEFAULT     p   ALTER TABLE ONLY reversion_revision ALTER COLUMN id SET DEFAULT nextval('reversion_revision_id_seq'::regclass);
 D   ALTER TABLE public.reversion_revision ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    219    220    220            y           2604    77216    reversion_version id    DEFAULT     n   ALTER TABLE ONLY reversion_version ALTER COLUMN id SET DEFAULT nextval('reversion_version_id_seq'::regclass);
 C   ALTER TABLE public.reversion_version ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    221    222    222            }           2604    77303    venta_arqueo id    DEFAULT     d   ALTER TABLE ONLY venta_arqueo ALTER COLUMN id SET DEFAULT nextval('venta_arqueo_id_seq'::regclass);
 >   ALTER TABLE public.venta_arqueo ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    230    231            {           2604    77269    venta_detalleventa id    DEFAULT     p   ALTER TABLE ONLY venta_detalleventa ALTER COLUMN id SET DEFAULT nextval('venta_detalleventa_id_seq'::regclass);
 D   ALTER TABLE public.venta_detalleventa ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    226    227    227            |           2604    77277    venta_venta id    DEFAULT     b   ALTER TABLE ONLY venta_venta ALTER COLUMN id SET DEFAULT nextval('venta_venta_id_seq'::regclass);
 =   ALTER TABLE public.venta_venta ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228    229            �	          0    76915    account_emailaddress 
   TABLE DATA               P   COPY account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
    public       postgres    false    202   �`      �	           0    0    account_emailaddress_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('account_emailaddress_id_seq', 1, false);
            public       postgres    false    201            �	          0    76925    account_emailconfirmation 
   TABLE DATA               V   COPY account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
    public       postgres    false    204   a      �	           0    0     account_emailconfirmation_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('account_emailconfirmation_id_seq', 1, false);
            public       postgres    false    203            y	          0    76819 
   auth_group 
   TABLE DATA               4   COPY auth_group (id, name, description) FROM stdin;
    public       postgres    false    192   2a      �	           0    0    auth_group_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('auth_group_id_seq', 1, false);
            public       postgres    false    191            {	          0    76829    auth_group_permissions 
   TABLE DATA               F   COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public       postgres    false    194   Oa      �	           0    0    auth_group_permissions_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);
            public       postgres    false    193            w	          0    76811    auth_permission 
   TABLE DATA               G   COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
    public       postgres    false    190   la      �	           0    0    auth_permission_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('auth_permission_id_seq', 60, true);
            public       postgres    false    189            }	          0    76837 	   auth_user 
   TABLE DATA               �   COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public       postgres    false    196   
d      	          0    76847    auth_user_groups 
   TABLE DATA               :   COPY auth_user_groups (id, user_id, group_id) FROM stdin;
    public       postgres    false    198   �d      �	           0    0    auth_user_groups_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);
            public       postgres    false    197            �	           0    0    auth_user_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('auth_user_id_seq', 1, true);
            public       postgres    false    195            �	          0    76855    auth_user_user_permissions 
   TABLE DATA               I   COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public       postgres    false    200   �d      �	           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);
            public       postgres    false    199            �	          0    76997    combustibles_combustible 
   TABLE DATA               N   COPY combustibles_combustible (id, octanaje, stock, stock_minimo) FROM stdin;
    public       postgres    false    208   �d      �	           0    0    combustibles_combustible_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('combustibles_combustible_id_seq', 1, false);
            public       postgres    false    207            �	          0    77075    compra_compra 
   TABLE DATA               �   COPY compra_compra (id, fecha_emision, fecha_descarga, patente, nombre_chofer, oc93, oc97, diesel, precio_id, estado) FROM stdin;
    public       postgres    false    212   )e      �	           0    0    compra_compra_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('compra_compra_id_seq', 1, false);
            public       postgres    false    211            �	          0    76957    django_admin_log 
   TABLE DATA               �   COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public       postgres    false    206   Fe      �	           0    0    django_admin_log_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);
            public       postgres    false    205            u	          0    76801    django_content_type 
   TABLE DATA               <   COPY django_content_type (id, app_label, model) FROM stdin;
    public       postgres    false    188   ce      �	           0    0    django_content_type_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);
            public       postgres    false    187            s	          0    76790    django_migrations 
   TABLE DATA               <   COPY django_migrations (id, app, name, applied) FROM stdin;
    public       postgres    false    186   Uf      �	           0    0    django_migrations_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('django_migrations_id_seq', 65, true);
            public       postgres    false    185            �	          0    77243    django_session 
   TABLE DATA               I   COPY django_session (session_key, session_data, expire_date) FROM stdin;
    public       postgres    false    223   �j      �	          0    77255    django_site 
   TABLE DATA               0   COPY django_site (id, domain, name) FROM stdin;
    public       postgres    false    225   �k      �	           0    0    django_site_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('django_site_id_seq', 1, true);
            public       postgres    false    224            �	          0    77112    handling_handlingcliente 
   TABLE DATA               T   COPY handling_handlingcliente (id, nombre, rut, telefono, stock_litros) FROM stdin;
    public       postgres    false    214   %l      �	           0    0    handling_handlingcliente_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('handling_handlingcliente_id_seq', 1, false);
            public       postgres    false    213            �	          0    77120    handling_handlingdiario 
   TABLE DATA               v   COPY handling_handlingdiario (id, fecha, kilometraje, patente, nombre_chofer, litros, handlingcliente_id) FROM stdin;
    public       postgres    false    216   Bl      �	           0    0    handling_handlingdiario_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('handling_handlingdiario_id_seq', 1, false);
            public       postgres    false    215            �	          0    77181    handling_ingresohandling 
   TABLE DATA               b   COPY handling_ingresohandling (id, fecha, numero_guia, recepcion, handlingcliente_id) FROM stdin;
    public       postgres    false    218   _l      �	           0    0    handling_ingresohandling_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('handling_ingresohandling_id_seq', 1, false);
            public       postgres    false    217            �	          0    77005    precio_preciohistorico 
   TABLE DATA               ;  COPY precio_preciohistorico (id, fechaini, fechafin, costo93, iva93, impfijo93, impvar93, imptotal93, oc93, costo95, iva95, impfijo95, impvar95, imptotal95, oc95, costo97, iva97, impfijo97, impvar97, imptotal97, oc97, costodiesel, ivadiesel, impfijodiesel, impvardiesel, imptotaldiesel, diesel, margen) FROM stdin;
    public       postgres    false    210   |l      �	           0    0    precio_preciohistorico_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('precio_preciohistorico_id_seq', 1, false);
            public       postgres    false    209            �	          0    77202    reversion_revision 
   TABLE DATA               I   COPY reversion_revision (id, date_created, comment, user_id) FROM stdin;
    public       postgres    false    220   �l      �	           0    0    reversion_revision_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('reversion_revision_id_seq', 1, false);
            public       postgres    false    219            �	          0    77213    reversion_version 
   TABLE DATA               {   COPY reversion_version (id, object_id, format, serialized_data, object_repr, content_type_id, revision_id, db) FROM stdin;
    public       postgres    false    222   �l      �	           0    0    reversion_version_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('reversion_version_id_seq', 1, false);
            public       postgres    false    221            �	          0    77300    venta_arqueo 
   TABLE DATA               �   COPY venta_arqueo (id, tot_efectivo, cheque, credito, tarjecredebtbk, tarjedebtbk, subtotaltarje, tot_ingreso, venta_id) FROM stdin;
    public       postgres    false    231   �l      �	           0    0    venta_arqueo_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('venta_arqueo_id_seq', 1, false);
            public       postgres    false    230            �	          0    77266    venta_detalleventa 
   TABLE DATA               T   COPY venta_detalleventa (id, tot_venta_litro, combustible_id, venta_id) FROM stdin;
    public       postgres    false    227   m      �	           0    0    venta_detalleventa_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('venta_detalleventa_id_seq', 1, false);
            public       postgres    false    226            �	          0    77274    venta_venta 
   TABLE DATA               C   COPY venta_venta (id, fecha, turno, precio_id, estado) FROM stdin;
    public       postgres    false    229   .m      �	           0    0    venta_venta_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('venta_venta_id_seq', 1, false);
            public       postgres    false    228            �           2606    76948 3   account_emailaddress account_emailaddress_email_key 
   CONSTRAINT     h   ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);
 ]   ALTER TABLE ONLY public.account_emailaddress DROP CONSTRAINT account_emailaddress_email_key;
       public         postgres    false    202    202            �           2606    76920 .   account_emailaddress account_emailaddress_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.account_emailaddress DROP CONSTRAINT account_emailaddress_pkey;
       public         postgres    false    202    202            �           2606    76932 ;   account_emailconfirmation account_emailconfirmation_key_key 
   CONSTRAINT     n   ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);
 e   ALTER TABLE ONLY public.account_emailconfirmation DROP CONSTRAINT account_emailconfirmation_key_key;
       public         postgres    false    204    204            �           2606    76930 8   account_emailconfirmation account_emailconfirmation_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);
 b   ALTER TABLE ONLY public.account_emailconfirmation DROP CONSTRAINT account_emailconfirmation_pkey;
       public         postgres    false    204    204            �           2606    76826    auth_group auth_group_name_key 
   CONSTRAINT     R   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public         postgres    false    192    192            �           2606    76881 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public         postgres    false    194    194    194            �           2606    76834 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public         postgres    false    194    194            �           2606    76824    auth_group auth_group_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public         postgres    false    192    192            �           2606    76867 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public         postgres    false    190    190    190            �           2606    76816 $   auth_permission auth_permission_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public         postgres    false    190    190            �           2606    76852 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public         postgres    false    198    198            �           2606    76896 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public         postgres    false    198    198    198            �           2606    76842    auth_user auth_user_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public         postgres    false    196    196            �           2606    76860 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public         postgres    false    200    200            �           2606    76910 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public         postgres    false    200    200    200            �           2606    76980     auth_user auth_user_username_key 
   CONSTRAINT     X   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public         postgres    false    196    196            �           2606    77002 6   combustibles_combustible combustibles_combustible_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY combustibles_combustible
    ADD CONSTRAINT combustibles_combustible_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.combustibles_combustible DROP CONSTRAINT combustibles_combustible_pkey;
       public         postgres    false    208    208            �           2606    77080     compra_compra compra_compra_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY compra_compra
    ADD CONSTRAINT compra_compra_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.compra_compra DROP CONSTRAINT compra_compra_pkey;
       public         postgres    false    212    212            �           2606    76966 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public         postgres    false    206    206            �           2606    76808 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public         postgres    false    188    188    188            �           2606    76806 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public         postgres    false    188    188                       2606    76798 (   django_migrations django_migrations_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public         postgres    false    186    186            �           2606    77250 "   django_session django_session_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public         postgres    false    223    223            �           2606    77262 ,   django_site django_site_domain_a2e37b91_uniq 
   CONSTRAINT     b   ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);
 V   ALTER TABLE ONLY public.django_site DROP CONSTRAINT django_site_domain_a2e37b91_uniq;
       public         postgres    false    225    225            �           2606    77260    django_site django_site_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.django_site DROP CONSTRAINT django_site_pkey;
       public         postgres    false    225    225            �           2606    77117 6   handling_handlingcliente handling_handlingcliente_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY handling_handlingcliente
    ADD CONSTRAINT handling_handlingcliente_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.handling_handlingcliente DROP CONSTRAINT handling_handlingcliente_pkey;
       public         postgres    false    214    214            �           2606    77125 4   handling_handlingdiario handling_handlingdiario_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY handling_handlingdiario
    ADD CONSTRAINT handling_handlingdiario_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.handling_handlingdiario DROP CONSTRAINT handling_handlingdiario_pkey;
       public         postgres    false    216    216            �           2606    77186 6   handling_ingresohandling handling_ingresohandling_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY handling_ingresohandling
    ADD CONSTRAINT handling_ingresohandling_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.handling_ingresohandling DROP CONSTRAINT handling_ingresohandling_pkey;
       public         postgres    false    218    218            �           2606    77010 2   precio_preciohistorico precio_preciohistorico_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY precio_preciohistorico
    ADD CONSTRAINT precio_preciohistorico_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.precio_preciohistorico DROP CONSTRAINT precio_preciohistorico_pkey;
       public         postgres    false    210    210            �           2606    77210 *   reversion_revision reversion_revision_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY reversion_revision
    ADD CONSTRAINT reversion_revision_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.reversion_revision DROP CONSTRAINT reversion_revision_pkey;
       public         postgres    false    220    220            �           2606    77223 J   reversion_version reversion_version_db_content_type_id_objec_b2c54f65_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY reversion_version
    ADD CONSTRAINT reversion_version_db_content_type_id_objec_b2c54f65_uniq UNIQUE (db, content_type_id, object_id, revision_id);
 t   ALTER TABLE ONLY public.reversion_version DROP CONSTRAINT reversion_version_db_content_type_id_objec_b2c54f65_uniq;
       public         postgres    false    222    222    222    222    222            �           2606    77221 (   reversion_version reversion_version_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY reversion_version
    ADD CONSTRAINT reversion_version_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.reversion_version DROP CONSTRAINT reversion_version_pkey;
       public         postgres    false    222    222            �           2606    77305    venta_arqueo venta_arqueo_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY venta_arqueo
    ADD CONSTRAINT venta_arqueo_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.venta_arqueo DROP CONSTRAINT venta_arqueo_pkey;
       public         postgres    false    231    231            �           2606    77271 *   venta_detalleventa venta_detalleventa_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY venta_detalleventa
    ADD CONSTRAINT venta_detalleventa_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.venta_detalleventa DROP CONSTRAINT venta_detalleventa_pkey;
       public         postgres    false    227    227            �           2606    77279    venta_venta venta_venta_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY venta_venta
    ADD CONSTRAINT venta_venta_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.venta_venta DROP CONSTRAINT venta_venta_pkey;
       public         postgres    false    229    229            �           1259    76949 (   account_emailaddress_email_03be32b2_like    INDEX     w   CREATE INDEX account_emailaddress_email_03be32b2_like ON account_emailaddress USING btree (email varchar_pattern_ops);
 <   DROP INDEX public.account_emailaddress_email_03be32b2_like;
       public         postgres    false    202            �           1259    76939 %   account_emailaddress_user_id_2c513194    INDEX     b   CREATE INDEX account_emailaddress_user_id_2c513194 ON account_emailaddress USING btree (user_id);
 9   DROP INDEX public.account_emailaddress_user_id_2c513194;
       public         postgres    false    202            �           1259    76946 3   account_emailconfirmation_email_address_id_5b7f8c58    INDEX     ~   CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON account_emailconfirmation USING btree (email_address_id);
 G   DROP INDEX public.account_emailconfirmation_email_address_id_5b7f8c58;
       public         postgres    false    204            �           1259    76945 +   account_emailconfirmation_key_f43612bd_like    INDEX     }   CREATE INDEX account_emailconfirmation_key_f43612bd_like ON account_emailconfirmation USING btree (key varchar_pattern_ops);
 ?   DROP INDEX public.account_emailconfirmation_key_f43612bd_like;
       public         postgres    false    204            �           1259    76869    auth_group_name_a6ea08ec_like    INDEX     a   CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public         postgres    false    192            �           1259    76882 (   auth_group_permissions_group_id_b120cbf9    INDEX     h   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public         postgres    false    194            �           1259    76883 -   auth_group_permissions_permission_id_84c5c92e    INDEX     r   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public         postgres    false    194            �           1259    76868 (   auth_permission_content_type_id_2f476e4b    INDEX     h   CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public         postgres    false    190            �           1259    76898 "   auth_user_groups_group_id_97559544    INDEX     \   CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public         postgres    false    198            �           1259    76897 !   auth_user_groups_user_id_6a12ed8b    INDEX     Z   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public         postgres    false    198            �           1259    76912 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     z   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public         postgres    false    200            �           1259    76911 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     n   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public         postgres    false    200            �           1259    76981     auth_user_username_6821ab7c_like    INDEX     g   CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public         postgres    false    196            �           1259    77090     compra_compra_precio_id_a0d9621f    INDEX     X   CREATE INDEX compra_compra_precio_id_a0d9621f ON compra_compra USING btree (precio_id);
 4   DROP INDEX public.compra_compra_precio_id_a0d9621f;
       public         postgres    false    212            �           1259    76977 )   django_admin_log_content_type_id_c4bce8eb    INDEX     j   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public         postgres    false    206            �           1259    76978 !   django_admin_log_user_id_c564eba6    INDEX     Z   CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public         postgres    false    206            �           1259    77252 #   django_session_expire_date_a5c62663    INDEX     ^   CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public         postgres    false    223            �           1259    77251 (   django_session_session_key_c0390e0f_like    INDEX     w   CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public         postgres    false    223            �           1259    77263     django_site_domain_a2e37b91_like    INDEX     g   CREATE INDEX django_site_domain_a2e37b91_like ON django_site USING btree (domain varchar_pattern_ops);
 4   DROP INDEX public.django_site_domain_a2e37b91_like;
       public         postgres    false    225            �           1259    77131 3   handling_handlingdiario_handlingcliente_id_f817ffe1    INDEX     ~   CREATE INDEX handling_handlingdiario_handlingcliente_id_f817ffe1 ON handling_handlingdiario USING btree (handlingcliente_id);
 G   DROP INDEX public.handling_handlingdiario_handlingcliente_id_f817ffe1;
       public         postgres    false    216            �           1259    77192 4   handling_ingresohandling_handlingcliente_id_89109415    INDEX     �   CREATE INDEX handling_ingresohandling_handlingcliente_id_89109415 ON handling_ingresohandling USING btree (handlingcliente_id);
 H   DROP INDEX public.handling_ingresohandling_handlingcliente_id_89109415;
       public         postgres    false    218            �           1259    77229 (   reversion_revision_date_created_96f7c20c    INDEX     h   CREATE INDEX reversion_revision_date_created_96f7c20c ON reversion_revision USING btree (date_created);
 <   DROP INDEX public.reversion_revision_date_created_96f7c20c;
       public         postgres    false    220            �           1259    77230 #   reversion_revision_user_id_17095f45    INDEX     ^   CREATE INDEX reversion_revision_user_id_17095f45 ON reversion_revision USING btree (user_id);
 7   DROP INDEX public.reversion_revision_user_id_17095f45;
       public         postgres    false    220            �           1259    77241 *   reversion_version_content_type_id_7d0ff25c    INDEX     l   CREATE INDEX reversion_version_content_type_id_7d0ff25c ON reversion_version USING btree (content_type_id);
 >   DROP INDEX public.reversion_version_content_type_id_7d0ff25c;
       public         postgres    false    222            �           1259    77242 &   reversion_version_revision_id_af9f6a9d    INDEX     d   CREATE INDEX reversion_version_revision_id_af9f6a9d ON reversion_version USING btree (revision_id);
 :   DROP INDEX public.reversion_version_revision_id_af9f6a9d;
       public         postgres    false    222            �           1259    77306    venta_arqueo_venta_id_24840d7f    INDEX     T   CREATE INDEX venta_arqueo_venta_id_24840d7f ON venta_arqueo USING btree (venta_id);
 2   DROP INDEX public.venta_arqueo_venta_id_24840d7f;
       public         postgres    false    231            �           1259    77285 *   venta_detalleventa_combustible_id_73b9f157    INDEX     l   CREATE INDEX venta_detalleventa_combustible_id_73b9f157 ON venta_detalleventa USING btree (combustible_id);
 >   DROP INDEX public.venta_detalleventa_combustible_id_73b9f157;
       public         postgres    false    227            �           1259    77292 $   venta_detalleventa_venta_id_66610f20    INDEX     `   CREATE INDEX venta_detalleventa_venta_id_66610f20 ON venta_detalleventa USING btree (venta_id);
 8   DROP INDEX public.venta_detalleventa_venta_id_66610f20;
       public         postgres    false    227            �           1259    77291    venta_venta_precio_id_32d4c83b    INDEX     T   CREATE INDEX venta_venta_precio_id_32d4c83b ON venta_venta USING btree (precio_id);
 2   DROP INDEX public.venta_venta_precio_id_32d4c83b;
       public         postgres    false    229            �           2620    77338    venta_arqueo addid    TRIGGER     `   CREATE TRIGGER addid AFTER INSERT ON venta_arqueo FOR EACH STATEMENT EXECUTE PROCEDURE addid();
 +   DROP TRIGGER addid ON public.venta_arqueo;
       public       postgres    false    249    231            �           2620    77330    compra_compra eliminarcompra    TRIGGER     m   CREATE TRIGGER eliminarcompra AFTER UPDATE ON compra_compra FOR EACH ROW EXECUTE PROCEDURE eliminarcompra();
 5   DROP TRIGGER eliminarcompra ON public.compra_compra;
       public       postgres    false    245    212            �           2620    77340    venta_venta eliminarventa    TRIGGER     i   CREATE TRIGGER eliminarventa AFTER UPDATE ON venta_venta FOR EACH ROW EXECUTE PROCEDURE eliminarventa();
 2   DROP TRIGGER eliminarventa ON public.venta_venta;
       public       postgres    false    250    229            �           2620    77336 #   venta_detalleventa restacombustible    TRIGGER     }   CREATE TRIGGER restacombustible AFTER INSERT ON venta_detalleventa FOR EACH STATEMENT EXECUTE PROCEDURE quitarcombustible();
 <   DROP TRIGGER restacombustible ON public.venta_detalleventa;
       public       postgres    false    248    227            �           2620    77332 *   handling_handlingdiario restastockhandling    TRIGGER     �   CREATE TRIGGER restastockhandling AFTER INSERT ON handling_handlingdiario FOR EACH STATEMENT EXECUTE PROCEDURE handlingdescuento();
 C   DROP TRIGGER restastockhandling ON public.handling_handlingdiario;
       public       postgres    false    216    246            �           2620    77328    compra_compra sumarcombustible    TRIGGER     z   CREATE TRIGGER sumarcombustible AFTER INSERT ON compra_compra FOR EACH STATEMENT EXECUTE PROCEDURE agreagarcombustible();
 7   DROP TRIGGER sumarcombustible ON public.compra_compra;
       public       postgres    false    244    212            �           2620    77334 +   handling_ingresohandling sumarstockhandling    TRIGGER     �   CREATE TRIGGER sumarstockhandling AFTER INSERT ON handling_ingresohandling FOR EACH STATEMENT EXECUTE PROCEDURE handlingingreso();
 D   DROP TRIGGER sumarstockhandling ON public.handling_ingresohandling;
       public       postgres    false    218    247            �           2606    76985 =   account_emailaddress account_emailaddress_user_id_2c513194_fk    FK CONSTRAINT     �   ALTER TABLE ONLY account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 g   ALTER TABLE ONLY public.account_emailaddress DROP CONSTRAINT account_emailaddress_user_id_2c513194_fk;
       public       postgres    false    196    202    2197            �           2606    76950 P   account_emailconfirmation account_emailconfirmation_email_address_id_5b7f8c58_fk    FK CONSTRAINT     �   ALTER TABLE ONLY account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_email_address_id_5b7f8c58_fk FOREIGN KEY (email_address_id) REFERENCES account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.account_emailconfirmation DROP CONSTRAINT account_emailconfirmation_email_address_id_5b7f8c58_fk;
       public       postgres    false    202    204    2217            �           2606    76875 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public       postgres    false    194    2184    190            �           2606    76870 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public       postgres    false    194    2189    192            �           2606    76861 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public       postgres    false    188    190    2179            �           2606    76890 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public       postgres    false    198    192    2189            �           2606    76885 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public       postgres    false    2197    198    196            �           2606    76904 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public       postgres    false    200    190    2184            �           2606    76899 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public       postgres    false    2197    196    200            �           2606    77091 K   compra_compra compra_compra_precio_id_a0d9621f_fk_precio_preciohistorico_id    FK CONSTRAINT     �   ALTER TABLE ONLY compra_compra
    ADD CONSTRAINT compra_compra_precio_id_a0d9621f_fk_precio_preciohistorico_id FOREIGN KEY (precio_id) REFERENCES precio_preciohistorico(id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY public.compra_compra DROP CONSTRAINT compra_compra_precio_id_a0d9621f_fk_precio_preciohistorico_id;
       public       postgres    false    2232    212    210            �           2606    76967 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public       postgres    false    188    2179    206            �           2606    76990 5   django_admin_log django_admin_log_user_id_c564eba6_fk    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 _   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk;
       public       postgres    false    206    196    2197            �           2606    77168 U   handling_handlingdiario handling_handlingdia_handlingcliente_id_f817ffe1_fk_handling_    FK CONSTRAINT     �   ALTER TABLE ONLY handling_handlingdiario
    ADD CONSTRAINT handling_handlingdia_handlingcliente_id_f817ffe1_fk_handling_ FOREIGN KEY (handlingcliente_id) REFERENCES handling_handlingcliente(id) DEFERRABLE INITIALLY DEFERRED;
    ALTER TABLE ONLY public.handling_handlingdiario DROP CONSTRAINT handling_handlingdia_handlingcliente_id_f817ffe1_fk_handling_;
       public       postgres    false    2237    216    214            �           2606    77187 V   handling_ingresohandling handling_ingresohand_handlingcliente_id_89109415_fk_handling_    FK CONSTRAINT     �   ALTER TABLE ONLY handling_ingresohandling
    ADD CONSTRAINT handling_ingresohand_handlingcliente_id_89109415_fk_handling_ FOREIGN KEY (handlingcliente_id) REFERENCES handling_handlingcliente(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.handling_ingresohandling DROP CONSTRAINT handling_ingresohand_handlingcliente_id_89109415_fk_handling_;
       public       postgres    false    2237    214    218            �           2606    77224 F   reversion_revision reversion_revision_user_id_17095f45_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY reversion_revision
    ADD CONSTRAINT reversion_revision_user_id_17095f45_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.reversion_revision DROP CONSTRAINT reversion_revision_user_id_17095f45_fk_auth_user_id;
       public       postgres    false    196    2197    220            �           2606    77231 I   reversion_version reversion_version_content_type_id_7d0ff25c_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY reversion_version
    ADD CONSTRAINT reversion_version_content_type_id_7d0ff25c_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.reversion_version DROP CONSTRAINT reversion_version_content_type_id_7d0ff25c_fk_django_co;
       public       postgres    false    222    188    2179            �           2606    77236 Q   reversion_version reversion_version_revision_id_af9f6a9d_fk_reversion_revision_id    FK CONSTRAINT     �   ALTER TABLE ONLY reversion_version
    ADD CONSTRAINT reversion_version_revision_id_af9f6a9d_fk_reversion_revision_id FOREIGN KEY (revision_id) REFERENCES reversion_revision(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY public.reversion_version DROP CONSTRAINT reversion_version_revision_id_af9f6a9d_fk_reversion_revision_id;
       public       postgres    false    2246    222    220            �           2606    77307 =   venta_arqueo venta_arqueo_venta_id_24840d7f_fk_venta_venta_id    FK CONSTRAINT     �   ALTER TABLE ONLY venta_arqueo
    ADD CONSTRAINT venta_arqueo_venta_id_24840d7f_fk_venta_venta_id FOREIGN KEY (venta_id) REFERENCES venta_venta(id) DEFERRABLE INITIALLY DEFERRED;
 g   ALTER TABLE ONLY public.venta_arqueo DROP CONSTRAINT venta_arqueo_venta_id_24840d7f_fk_venta_venta_id;
       public       postgres    false    2268    229    231            �           2606    77280 J   venta_detalleventa venta_detalleventa_combustible_id_73b9f157_fk_combustib    FK CONSTRAINT     �   ALTER TABLE ONLY venta_detalleventa
    ADD CONSTRAINT venta_detalleventa_combustible_id_73b9f157_fk_combustib FOREIGN KEY (combustible_id) REFERENCES combustibles_combustible(id) DEFERRABLE INITIALLY DEFERRED;
 t   ALTER TABLE ONLY public.venta_detalleventa DROP CONSTRAINT venta_detalleventa_combustible_id_73b9f157_fk_combustib;
       public       postgres    false    2230    208    227            �           2606    77293 I   venta_detalleventa venta_detalleventa_venta_id_66610f20_fk_venta_venta_id    FK CONSTRAINT     �   ALTER TABLE ONLY venta_detalleventa
    ADD CONSTRAINT venta_detalleventa_venta_id_66610f20_fk_venta_venta_id FOREIGN KEY (venta_id) REFERENCES venta_venta(id) DEFERRABLE INITIALLY DEFERRED;
 s   ALTER TABLE ONLY public.venta_detalleventa DROP CONSTRAINT venta_detalleventa_venta_id_66610f20_fk_venta_venta_id;
       public       postgres    false    229    227    2268            �           2606    77286 G   venta_venta venta_venta_precio_id_32d4c83b_fk_precio_preciohistorico_id    FK CONSTRAINT     �   ALTER TABLE ONLY venta_venta
    ADD CONSTRAINT venta_venta_precio_id_32d4c83b_fk_precio_preciohistorico_id FOREIGN KEY (precio_id) REFERENCES precio_preciohistorico(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.venta_venta DROP CONSTRAINT venta_venta_precio_id_32d4c83b_fk_precio_preciohistorico_id;
       public       postgres    false    210    2232    229            �	      x������ � �      �	      x������ � �      y	      x������ � �      {	      x������ � �      w	   �  x�m�]��0��ݧ�	F���<�5VZ1���D k�Hs�5v��6���+W��H�k�/�8^��q��f���ǝ��G����F�=8�Io�6����Ӭ�Yf��k@K}�Ƅ��5cћ�������%jo쟡���5��F�"�&��U2x�ڊ�߿?���~|��@����P���˼��y�~^Z��G�dC�
=�R����R�U����xy�2��	�o��2a�lg6-���=���Qx]< �E ��P�r�̴?Yg,z��g8US�G[�h�}��\�y܇�2�9l����Pt�;&+�4�^�-ʵ}Jk����?�{�xu��(��YW4	%1@&e���6P���5�.�i�Xu-א�{~���|NZH�ۗFP�|������>Ƨ�#|n1w�8��q�O�-�S�F�sC���L��B�ygBX
��r4�5������ӈYS$�2u0ˊ|Q�����-I+d(T��14�T�T�� d�z&Ča�ٗ�w�Df�k��/��i+�3U�-���τ�״�r�6LӾD�6j�~-���ٸ��A���D�r�\����ּH�����2�<��z����}����-�䌠Q�X�f
�"���y�A�*��;A{+*��6;�UE� �;f�~ �#6p�      }	   �   x�3�,H�NI3�/�H425S16300P1v��pMˮ��VIqO3	.��uu��Hw���qN��2rO,t�w.����	��4pKJ��N��420��54�50V00�25�22�34574� 
q�p&��f�qA	�*61�2��3�4470
q��qqq y(z      	      x������ � �      �	      x������ � �      �	   .   x�3�4�4�4200�2�4��M8S2S�Ss`|cNKs;F��� W	�      �	      x������ � �      �	      x������ � �      u	   �   x�e�Mn1��p�*������Ǟb;Rn_f⤩���{��s@a�1���q��3�$�HN�{(��6�����|N�l�>sy<B��]���H����3����
<�D
Am/e��%:Q]�n�7�����zչ7�Wt[3][�r�k����H)DIë�Q,����gAH%�;���Y��dtG����^F)5�xs�,S��+��+�;w��#�w���xV�??����      s	   o  x���ю�:���S�,���I�YV��u�Z� �}�c�N�iF�7Ո���8�o�c׎u;�o�P(� 4m36ե@�'�OE?����z yt����o�
ǈfzt��:�{;n�H��qv�C}��K�V��Kݞ��95Y�=OjST�k�n��0*�Q�,�p�Β��o��k�Q9r��)ϰ^�G��U�$<��7���uŀ�E�{�Cu�>����CӵQ��F���P����>�Ǯ��F�f��zb�3}�x��8��+��d�%�R��F�v��o���"e.A���ߛ��ߘ�0e���J�zB�#���Ҝ����C��]��0T�:2�P��~�\6;yv�)�̓�ù��p��c��F�����}�T���}�?�;�'y�N`�[_�n��1��"�aj�I W#`��$�J�������Q���(�9�6�鼷��
ZD,�$��hw�R���C#.����XʕJk���~Z�uH�GrRK�ǢE�P�Oe=��)�t�y��6)/�&��zk=E?F^��B/��k��{�1�>��jO��=o�� }����[�v����1
Я/E&��%8C���V�i�OM�7]�K��dm�`�2/��'Xg9pEX�$@ �:�2���=G�"�},�4OH� n	@���K)�"���ԥ��c
/���Vt)5�mB�j��<���=�䤕7)��&�6��t�����3��	�*Z��)�<�2k)։^-�/V2G`��"��U0�<C2�HѫZ��B�����:�+#>y���� ��)�~��?�~�i���%�������<���E_����m����٫h�ڭu�mj@�-�3=A���x�W��Y��F��A�W���Z#�1yf�jB�A�(�:��)�f��`14���år�]�Ь�\�O���m����=�������]|Hoon",� [J3k֣��Y���+m���򗹖��"'������z��ť��SI�f�� �NU[ʛ4���,�9�D�ڤ+f5���gZ/���B4tǪ��Z��&mS3b=������`%�qQg��~�A��	 6�S.�k������I�\n��]�bKgN�7���2�߫�>���:�y�<�G�|      �	     x�=��n�0 ���{M[��� ���Bv�hK�fq"<�p���×�_��;4�-�;.�{�����w:<uW�WD�2CU�i�c��M�Li���$'��]z�x��i����B튆%Wfgsɢ��cy��3��d��@���kW9�.s��U��MԦy�}�C2Uy�S�TG��Ck��ڏD��6�<���;���lbH��קM_�H�O\��I����Ґe�DOU?���#��)��
���hwo��;�m7pi�����Ʋ�_G�h      �	      x�3�L�H�-�I�K��Efs��qqq �<	�      �	      x������ � �      �	      x������ � �      �	      x������ � �      �	   )   x�3�4400�50"d�����PIN����q��qqq |RP      �	      x������ � �      �	      x������ � �      �	      x�3�4@�\1z\\\  ��      �	      x������ � �      �	   %   x�3�4400�50"NGNNϼ��̲D�=... a��     